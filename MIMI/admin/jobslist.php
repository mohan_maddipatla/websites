<?PHP
require_once("../api/include/Admin/appsettings_config.php");
require_once("../api/include/Admin/member_config.php");
require_once("../api/include/Admin/job_config.php");

if(!$member->CheckLogin())
{
    $member->RedirectToURL("login.php");
    exit;
}
else
{
	$job->getDepartments();
	$job->getLocations();
	$job->GetJobsFromDatabase();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>MiMi - Jobs</title>
    <!-- Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="MyInd MedTech Innovations Pvt Ltd is a technology based start-up organization created to offer a range of Health Care Services to Urban and Rural population.">
    <meta name="keywords" content="MyInd, MedTech, MyIndMedTech, Health, Care, Services, esahai, emergency, diagnostic, services, data analytics, EMS, Medical, Ambulance">
    <meta name="developer" content="">
    <meta name="robots" content="noindex">
    <!-- FAV AND TOUCH ICON -->
    <link rel="shortcut icon" href="../img/favicon.png">
    <!-- GOOGLE FONT -->
    <link href="../css/css.css" rel="stylesheet">

    <!-- FONT ICONS -->
    <link rel="stylesheet" href="../css/font-awesome.min.css">
    <!-- BOOTSTRAP CSS -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">

    <!-- ANIMATE CSS -->
    <link rel="stylesheet" href="../css/animate.css">

    <!-- fancybox CSS 
    <link rel="stylesheet" href="../css/jquery.fancybox.css">
        -->
    <!-- colorbox CSS-->
    <link rel="stylesheet" href="../css/colorbox.css">

    <!-- Works CSS-->
    <link rel="stylesheet" href="../css/nav.css">

    <!-- COUSTOM Style  -->
    <link rel="stylesheet" href="../css/style.css">

    <!-- Responsive css-->
    <link rel="stylesheet" href="../css/responsive.css">

    <!-- COLOR -->
    <link rel="stylesheet" href="../css/color-1.css" id="color-scheme">
    <!--<link rel="stylesheet" href="assets/css/color/color-2.css">-->
    <!--<link rel="stylesheet" href="assets/css/color/color-3.css">-->
    <!--<link rel="stylesheet" href="assets/css/color/color-4.css">-->

    <!-- roboto font -->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab" rel="stylesheet">

    <!--[if lt IE 9]>
        <script src="../js/html5shiv.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="admin.css">
    <style type="text/css">
        .fancybox-margin {
            margin-right: 17px;
        }
    </style>
</head>
<body data-spy="scroll" data-target=".navbar" data-offset="50">
    <!-- =========================
     PRE LOADER
    ============================== -->
    <div class="preloader" style="display: none;">
        <div class="status" style="display: none;">
            <div class="loader">
                <svg class="circular" viewBox="25 25 50 50">
                    <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10"></circle>
                </svg>
            </div>
        </div>
    </div>

    <!--
    |=================
    |       HEADER
    |=================
    -->
    <div id="sticker-sticky-wrapper" class="sticky-wrapper" style="height: 90px;">
        <header class="section navbar navbar-default navbar-fixed-top" id="sticker" style="width: 1349px;">
            <div class="container">
                <div class="row">
                    <nav class="">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="../index.html"><img src="../img/mm-logo.png" alt="MyInd" class="img-responsive"></a>
                        </div>
                        <div id="navbar" class="nav-collapse" aria-expanded="false">
                            <ul class="nav navbar-nav navbar-right" id="menuvar">
                            <li class="active">
                                <a href="">
                                    Jobs
                                </a>
                            </li>
                            <li class="">
                                <a href="profile.php">
                                    Profile
                                </a>
                            </li>
                            <li class="">
                                <a href="change_pwd.php">
                                    Change Password
                                </a>
                            </li>
                            <li class="">
                                <a href="logout.php">
                                    Logout
                                </a>
                            </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </header>
    </div>
        <!-- =========================
            Jobs List
        ============================== -->
        <section id="emergency-medical" class="app-star-feature-details-section">
            <div class="section-title">
                <div class="container">
                    <div class="row">
                        <!-- main title -->
                        <div class="col-md-12">
                            <h2>All jobs</h2>
                            <!-- <img src="./img/title_line.png" alt=""> -->
                        </div>
                    </div>
                </div>
            </div>
            <div id="section-features">
                <!-- feature AREA START HERE -->
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
							<!-- Form Code Start -->
							<div id='settingsDiv'>
								<div class="panel panel-default">
									<div class="panel-body">
										<div class="row" style="margin: 0px;">
											<div class="pull-right">
												<a id="addNewJob" class="btn btn-link"  data-toggle="modal" data-target="#modalAddJob" data-backdrop="static" data-keyboard="false">Add Job</a>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<table class="table table-hover" id="tableAllJobs">
													<thead>
														<tr>
															<th>Job Code</th>
															<th>Title</th>
															<th>Location</th>
															<th>Department</th>
															<th>Is Active</th>
															<th></th>
														</tr>
													</thead>
													<tbody
														<?php
															foreach($job->allJobs as $job1) { ?>
															<tr>
																<td><?php echo $job1[5] ?></td>
																<td><?php echo $job1[2] ?></td>
																<td><?php echo $job1[3] ?></td>
																<td><?php echo $job1[4] ?></td>
																<td>
																	<?php if($job1[1] == 0) echo 'False'; else echo 'True'; ?>
																</td>
																<td>
																	<a jobid="<?php echo $job1[0] ?>" href="#" class="selectJob">Edit</a>
																</td>
															</tr>
														  <?php
															} ?>
													</tbody>
												<table>
											</div>
										</div>
									</div>
								</div>
							</div>
                        </div>
                    </div>
                    <!-- end row -->
                </div>
                <!-- end container -->
            </div>
        </section>
        <!-- End Emergency medical services -->


    <div id="modalAddJob" class="modal fade generalModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <img class="close" data-dismiss="modal" src="../img/close-icon.png" alt="close">
                    <h4 class="modal-title">Add new Job</h4>
                </div>
                <div class="modal-body">
					<div class="row">
						<input type="hidden" id="jobId" name="jobId" value="">
						<div class="col-md-12">
							<input type="text" id="title" name="title" class="form-control" value='' placeholder="Title">
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<select id="location" name="location" class="form-control">
								<?php
									foreach($job->locations as $location) { ?>
									  <option value="<?php echo $location[0] ?>"><?php echo $location[0] ?></option>
								  <?php
									} ?>
							</select>
						</div>
						<div class="col-md-6">
							<select id="department" name="department" class="form-control">
								<?php
									foreach($job->departments as $department) { ?>
									  <option value="<?php echo $department[0] ?>"><?php echo $department[0] ?></option>
								  <?php
									} ?>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<textarea id="description" name="description" class="form-control" value='' placeholder="Description"></textarea>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<textarea id="requirements" name="requirements" class="form-control" value='' placeholder="Requirements"></textarea>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<input type="text" id="jobcode" name="jobcode" class="form-control" value='' placeholder="Job Code">
						</div>
						<div class="col-md-2">
						</div>
						<div class="col-md-4">
							Is Active:
							<input id="isactive" name="isactive" class="" type="checkbox" >
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<button id="addJob" class="btn btn-primary pull-right">Add</button>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					
				</div>
            </div>
        </div>
    </div>

    <!-- Jquery core JS-->
    <script src="../js/jquery-3.1.1.min.js"></script>

    <!-- Bootstrap JS-->
    <script src="../js/bootstrap.min.js"></script>

    <!-- mixin js-->
    <script src="../js/jquery.mixitup.min.js"></script>

    <!-- Nav JS-->
    <script src="../js/jquery.nav.js"></script>

    <!-- wow JS-->
    <script src="../js/wow.min.js"></script>

    <!-- wow JS-->
    <script src="../js/jquery.sticky.js"></script>

    <!-- Validator JS-->
    <script src="../js/validator.min.js"></script>

    <!-- fancybox JS
    <script src="../js/jquery.fancybox.pack.js"></script>
        -->
    <!-- colorbox JS-->
    <script src="../js/jquery.colorbox-min.js"></script>

    <!-- smoothScroll JS-->
    <script src="../js/smooth-scroll.js"></script>

    <!-- Custom Script -->
    <script src="../js/init.js"></script>

    <script src="../js/admin.js"></script>
	<script type='text/javascript' src='../js/gen_validatorv4.js'></script>

	<script type="text/javascript">
	</script>

</body>
</html>