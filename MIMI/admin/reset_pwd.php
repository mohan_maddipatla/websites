<?PHP
	require_once("../api/include/Admin/member_config.php");
  require_once("../api/include/Exceptions.php");

	$errors = array(); //To store errors
	$form_data = array(); //Pass back the data to

	try {
		if (!empty($_POST['username'])) {
			$username = $_POST['username'];
      
			if($row = $member->ResetPasswordAndSendMail($username)) {
				$form_data['success'] = true;
    		$form_data['email'] = $row['email'];
    		$form_data['password'] = $row['password'];
			}
			else {
				$form_data['success'] = false;
    		$form_data['errors'] = 'Unable to reset password';
			}
		}
		else {
			$form_data['success'] = false;
    	$form_data['errors'] = "Please provide the username to reset password.";
		}
	}
	catch(Exception $e) {
		$form_data['success'] = false;
    $form_data['errors'] = "Problem resetting the password. Please contact the administrator.";
    $form_data['error_logs'] = $e->getMessage();
	}

    //Return the data back to form.php
    echo json_encode($form_data);

?>
