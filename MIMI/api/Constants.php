<?php
/**
 * App Constants
 */
define('Error', 'There is a problem in processing your request. Please try again later.');


/* Contact US */
define('CONTACT_MAIL_FROM', 'MyInd MedTech <info@myindmedtech.com>');
define('CONTACT_MAIL_TO', 'info@myindmedtech.com');
define('CONTACT_MAIL_SUBJECT', 'Contact mail from ');
define('CONTACT_REQUIRED', 'Please enter all the fields.');
define('CONTACT_EMAIL_INVALID', 'Please enter a valid email.');
define('CONTACT_PHONE_INVALID', 'Please enter a valid phone number.');
define('CONTACT_SUCCESS', 'Thank you for contacting us. We will be in touch with you very soon.');

/*admin */
define('ADMIN_EMAIL', 'MyInd MedTech <info@myindmedtech.com>');

/* job */
define('JOB_REQ', 'Please enter all the fields.');

?>
