<?php
    require_once 'include/Exceptions.php';
    require_once 'Constants.php';
    require_once 'include/Admin/job_config.php';
    
	  $errors = array(); //To store errors
    $form_data = array(); //Pass back the data to
    
    /*
    $job->jobId = $_POST['jobId'];
    $job->title = $_POST['title'];    // required
    $job->location = $_POST['location'];    // required
    $job->department = $_POST['department'];  // required
    $job->description = $_POST['description'];       // not required
    $job->requirements = $_POST['requirements'];   // required
    $job->jobcode = $_POST['jobcode'];   // required
    $job->isactive = $_POST['isactive'];   // required
    */
    
    /* Validate the form on server side */
    if (empty($_POST['title']) || 
        empty($_POST['location']) ||
        empty($_POST['department']) ||
        empty($_POST['description']) ||
        empty($_POST['jobcode']) ||
        empty($_POST['requirements'])) {
        $errors['error'] = JOB_REQ;
    }
    
    function clean_string($string) {
      $bad = array("content-type","bcc:","to:","cc:","href");
      return str_replace($bad,"",$string);
    }
    
    if (!empty($errors)) {
		  //If errors in validation
    	$form_data['success'] = false;
    	$form_data['errors']  = $errors;
    } 
    else { 
		  //If not, process the form, and return true on success
      
      try {
        if($job->AddJob()) 
        {
    	    $form_data['success'] = true;
        }
        else
        {
    	    $form_data['success'] = false;
          $errors['error'] = Error;
    	    $form_data['errors']  = $errors;
          $form_data['error_logs'] = $job->GetErrorMessage();
        }
       }
       catch(Exception $e) {
    	  $form_data['success'] = false;
        $errors['error'] = Error;
    	  $form_data['errors']  = $errors;
        $form_data['error_logs'] = $e->getMessage();
       }
    }
    
    //Return the data back to form.php
    echo json_encode($form_data);

?>