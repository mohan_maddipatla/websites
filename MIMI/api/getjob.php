<?php
    require_once 'include/Exceptions.php';
    require_once 'Constants.php';
    require_once 'include/Admin/job_config.php';
    
	  $errors = array(); //To store errors
    $form_data = array(); //Pass back the data to
    
    if (empty($_POST['jobid'])) { 
        $errors['error'] = "Please provide a valid jobid";
    }

    if (!empty($errors)) {
		//If errors in validation
    	$form_data['success'] = false;
    	$form_data['errors']  = $errors;
    } else {
      try {
        $jobid = $_POST['jobid'];
        if($job->GetJobDetailsFromDatabase($jobid)) 
        {
    	    $form_data['success'] = true;
          $form_data['post'] = $job->selectedJob;
        }
        else
        {
    	    $form_data['success'] = false;
          $errors['error'] = Error;
    	    $form_data['errors']  = $errors;
          $form_data['error_logs'] = $job->GetErrorMessage();
        }
       }
       catch(Exception $e) {
    	  $form_data['success'] = false;
        $errors['error'] = Error;
    	  $form_data['errors']  = $errors;
        $form_data['error_logs'] = $e->getMessage();
       }
    }
    
    //Return the data back to form.php
    echo json_encode($form_data);

?>