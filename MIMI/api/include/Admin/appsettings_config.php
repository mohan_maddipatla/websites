<?PHP
require_once("appsettings.php");
require_once(__DIR__."/../Config.php");

$appsettings = new AppSettings();

//Provide your database login details here:
//hostname, user name, password, database name and table name
//note that the script will create the table (for example, fgusers in this case)
//by itself on submitting register.php for the first time
$appsettings->InitDB(
        /*hostname*/ DB_HOST, 
        /*username*/ DB_USERNAME, 
        /*password*/ DB_PASSWORD, 
        /*database name*/ DB_NAME, 
        /*table name*/'notifications');

//For better security. Get a random string from this link: http://tinyurl.com/randstr
// and put it here
$appsettings->SetRandomKey('kuWFmYSXlU01ISd');

?>