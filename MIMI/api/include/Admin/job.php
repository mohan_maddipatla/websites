<?PHP
require_once("formvalidator.php");
require_once(__DIR__."/../../Constants.php");

class Job
{
    var $title;
    var $location;
    var $department;
    var $description;
    var $requirements;
    var $jobcode;
    var $isactive;
    
    var $database;
    var $tablename;
    var $connection;
    var $rand_key;
    var $sitename;
    var $appname;
    var $error_message;
    
    var $locations = array();
    var $departments = array();
    var $allJobs = array();
    var $selectedJob = array();
    
    var $membername;
    var $memberusername;
    //-----Initialization -------
    function job()
    {
        $this->sitename = 'myindmedtech.com';
        $this->rand_key = 'kuWFmYSXlU01ISd';
        $this->appname = 'MiMi';
    }
    
    function InitDB($host,$uname,$pwd,$database,$tablename)
    {
        $this->db_host  = $host;
        $this->username = $uname;
        $this->pwd  = $pwd;
        $this->database  = $database;
        $this->tablename = $tablename;
        
    }
    function SetAdminEmail($email)
    {
        $this->admin_email = $email;
    }
    
    function SetWebsiteName($sitename)
    {
        $this->sitename = $sitename;
    }
    
    function SetAppName($appname)
    {
        $this->appname = $appname;
    }
    
    function SetRandomKey($key)
    {
        $this->rand_key = $key;
    }
    
    //-------Main Operations ----------------------
    function AddJob()
    {
        $formvars = array();
        
        if(!$this->ValidateJobSubmission())
        {
            return false;
        }
        
        $this->CollectJobSubmission($formvars);
        
        if(!$this->SaveJobToDatabase($formvars))
        {
            return false;
        }
        
        return true;
    }

    
    function UserFullName()
    {
        return isset($_SESSION['name_of_user'])?$_SESSION['name_of_user']:'';
    }
    
    function UserEmail()
    {
        return isset($_SESSION['email_of_user'])?$_SESSION['email_of_user']:'';
    }
    
    function UserName()
    {
        return isset($_SESSION['username_of_user'])?$_SESSION['username_of_user']:'';
    }
    
    function getDepartments()
    {
      $this->GetDepartmentsFromDatabase();
    }
    
    function getLocations()
    {
      $this->GetLocationsFromDatabase();
    }
    function getAllJobs()
    {
      $this->GetJobsFromDatabase();
    }
    
    //-------Public Helper functions -------------
    function GetSelfScript()
    {
        return htmlentities($_SERVER['PHP_SELF']);
    }
    
    function GetSpamTrapInputName()
    {
        return 'sp'.md5('KHGdnbvsgst'.$this->rand_key);
    }
    
    function GetErrorMessage()
    {
        if(empty($this->error_message))
        {
            return '';
        }
        $errormsg = nl2br(htmlentities($this->error_message));
        return $errormsg;
    }    
    //-------Private Helper functions-----------
    
    function HandleError($err)
    {
        $this->error_message .= $err."\r\n";
    }
    
    function HandleDBError($err)
    {
        $this->HandleError($err."\r\n mysqlerror:".mysql_error());
    }
    
    
    function ValidateJobSubmission()
    {
        //This is a hidden input field. Humans won't fill this field.
        if(!empty($_POST[$this->GetSpamTrapInputName()]) )
        {
            //The proper error is not given intentionally
            $this->HandleError("Automated submission prevention: case 2 failed");
            return false;
        }
        
        $validator = new FormValidator();
        $validator->addValidation("title","req","Please fill in Title");
        $validator->addValidation("location","req","Please fill in Location");
        $validator->addValidation("department","req","Please fill in Department");
        $validator->addValidation("description","req","Please fill in Description");
        $validator->addValidation("requirements","req","Please fill in Requirements");
        $validator->addValidation("jobcode","req","Please fill in JobCode");
        
        if(!$validator->ValidateForm())
        {
            $error='';
            $error_hash = $validator->GetErrors();
            foreach($error_hash as $inpname => $inp_err)
            {
                $error .= $inpname.':'.$inp_err."\n";
            }
            $this->HandleError($error);
            return false;
        }        
        return true;
    }

    function CollectJobSubmission(&$formvars)
    {
        $formvars['title'] = $this->Sanitize($_POST['title']);
        $formvars['location'] = $this->Sanitize($_POST['location']);
        $formvars['department'] = $this->Sanitize($_POST['department']);
        $formvars['description'] = $this->Sanitize($_POST['description'], false);
        $formvars['requirements'] = $this->Sanitize($_POST['requirements'], false);
        $formvars['isactive'] = $this->Sanitize($_POST['isactive']);
        $formvars['jobcode'] = $this->Sanitize($_POST['jobcode']);
        $formvars['jobId'] = $this->Sanitize($_POST['jobId']);
    }
    
    function GetAbsoluteURLFolder()
    {
        $scriptFolder = (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on')) ? 'https://' : 'http://';
        $scriptFolder .= $_SERVER['HTTP_HOST'] . dirname($_SERVER['REQUEST_URI']);
        return $scriptFolder;
    }
    
    function SaveJobToDatabase(&$formvars)
    {
        if(!$this->DBLogin())
        {
            $this->HandleError("Database login failed!");
            return false;
        }
        
        if(isset($formvars['jobId']) && !empty($formvars['jobId']))
        {
          if(!$this->UpdateJobToDB($formvars))
          {
              $this->HandleError("Updating to Database failed!");
              return false;
          }
        }
        else
        {
          if(!$this->InsertJobIntoDB($formvars))
          {
              $this->HandleError("Inserting to Database failed!");
              return false;
          }
        }
        return true;
    }
    
    function DBLogin()
    {

        //$this->connection = mysql_connect($this->db_host,$this->username,$this->pwd);
        $this->connection = new mysqli($this->db_host, $this->username, $this->pwd, $this->database);

        if(!$this->connection)
        {   
            $this->HandleDBError("Database Login failed! Please make sure that the DB login credentials provided are correct");
            return false;
        }
        /*
        if(!mysql_select_db($this->database, $this->connection))
        {
            $this->HandleDBError('Failed to select database: '.$this->database.' Please make sure that the database name provided is correct');
            return false;
        }
        if(!mysql_query("SET NAMES 'UTF8'",$this->connection))
        {
            $this->HandleDBError('Error setting utf8 encoding');
            return false;
        }
        */
        return true;
    }    
    
    function UpdateJobToDB(&$formvars)
    {
        $qry = "update $this->tablename set title='" . $formvars['title'] . 
                "' , location = '"  . $formvars['location'] . 
                "' , department = '"  . $formvars['department'] . 
                "' , description = '"  . $formvars['description'] .
                "' , requirements = '"  . $formvars['requirements'] . 
                "' , is_enabled = '"  . $formvars['isactive'] .
                "' , jobcode = '"  . $formvars['jobcode'] .
                "' where  id='" . $formvars['jobId'] . "'";
        
        if(!mysqli_query( $this->connection, $qry))
        {
            $this->HandleDBError("Error updating the job \nquery:$qry");
            return false;
        }     
        return true;
    }

    function InsertJobIntoDB(&$formvars)
    {
        $insert_query = 'insert into ' . $this->tablename . '(is_enabled,title,location,department,description,requirements, jobcode) values ("' . $this->SanitizeForSQL($formvars['isactive']) . '", "' 
                        . $this->SanitizeForSQL($formvars['title']) . '", "' 
                        . $this->SanitizeForSQL($formvars['location']) . '", "' 
                        . $this->SanitizeForSQL($formvars['department']) . '", "' 
                        . $this->SanitizeForSQL($formvars['description']) . '", "' 
                        . $this->SanitizeForSQL($formvars['requirements']) . '", "' 
                        . $this->SanitizeForSQL($formvars['jobcode']) . '" )';      
        
        if(!mysqli_query( $this->connection, $insert_query))
        {
            $this->HandleDBError("Error inserting data to the table\nquery:$insert_query");
            return false;
        }
        return true;
    }
    
    function GetJobsFromDatabase()
    {
        $query = "select * from Jobs order by id";
        
        if(!$this->DBLogin())
        {
            $this->HandleError("Database login failed!");
            return false;
        }
        
        $result = mysqli_query($this->connection, $query);
        if(!$result)
        {
          return false;
        }
        if ($result->num_rows > 0) {
          while($row = $result->fetch_assoc()) {
            $this->allJobs[] = array($row["id"], $row["is_enabled"],$row["title"],$row["location"],$row["department"], $row["jobcode"]);
          }
        }
        return true;
    }
    
    function GetActiveJobsFromDatabase()
    {
        $query = "select * from Jobs where is_enabled = 1 order by id";
        
        if(!$this->DBLogin())
        {
            $this->HandleError("Database login failed!");
            return false;
        }
        
        $result = mysqli_query($this->connection, $query);
        if(!$result)
        {
          return false;
        }
        if ($result->num_rows > 0) {
          while($row = $result->fetch_assoc()) {
            $this->allJobs[] = array($row["id"], $row["title"], $row["location"], $row["department"], $row["description"], $row["requirements"], $row["jobcode"]);
          }
        }
        return true;
    }


    function GetJobDetailsFromDatabase($jobId)
    {
        $query = "select * from Jobs where id = " . $jobId;
        
        if(!$this->DBLogin())
        {
            $this->HandleError("Database login failed!");
            return false;
        }
        
        $result = mysqli_query($this->connection, $query);
        if(!$result)
        {
          return false;
        }
        if ($result->num_rows > 0) {
          while($row = $result->fetch_assoc()) {
            $this->selectedJob["id"] = $row["id"];
            $this->selectedJob["is_enabled"] = $row["is_enabled"];
            $this->selectedJob["title"] = $row["title"];
            $this->selectedJob["location"] = $row["location"];
            $this->selectedJob["department"] = $row["department"];
            $this->selectedJob["description"] = $row["description"];
            $this->selectedJob["requirements"] = $row["requirements"];
            $this->selectedJob["jobcode"] = $row["jobcode"];
            $this->selectedJob["crtd_on"] = $row["crtd_on"];
          }
        }
        return true;
    }


    function GetLocationsFromDatabase()
    {
        $query = "select * from Locations where is_enabled = '1' order by sort_order";
        if(!$this->DBLogin())
        {
            $this->HandleError("Database login failed!");
            return false;
        }
        
        $result = mysqli_query($this->connection, $query);
        if(!$result)
        {
          return false;
        }
        if ($result->num_rows > 0) {
          while($row = $result->fetch_assoc()) {
            $this->locations[] = array($row["location"]);
          }
        }
        return true;
    }

    function GetDepartmentsFromDatabase()
    {
        $query = "select * from Departments where is_enabled = '1' order by sort_order";
        if(!$this->DBLogin())
        {
            $this->HandleError("Database login failed!");
            return false;
        }
        
        $result = mysqli_query($this->connection, $query);
        if(!$result)
        {
          return false;
        }
        if ($result->num_rows > 0) {
          while($row = $result->fetch_assoc()) {
            $this->departments[] = array($row["department"]);
          }
        }
        return true;
    }
    
    
    function SanitizeForSQL($str)
    {
        if( function_exists( "mysqli_real_escape_string" ) )
        {
              $ret_str = mysqli_real_escape_string( $this->connection, $str );
        }
        else
        {
              $ret_str = addslashes( $str );
        }
        return $ret_str;
    }
    
 /*
    Sanitize() function removes any potential threat from the
    data submitted. Prevents email injections or any other hacker attempts.
    if $remove_nl is true, newline chracters are removed from the input.
    */
    function Sanitize($str,$remove_nl=true)
    {
        $str = $this->StripSlashes($str);

        if($remove_nl)
        {
            $injections = array('/(\n+)/i',
                '/(\r+)/i',
                '/(\t+)/i',
                '/(%0A+)/i',
                '/(%0D+)/i',
                '/(%08+)/i',
                '/(%09+)/i'
                );
            $str = preg_replace($injections,'',$str);
        }

        return $str;
    }    
    function StripSlashes($str)
    {
        if(get_magic_quotes_gpc())
        {
            $str = stripslashes($str);
        }
        return $str;
    }    
}
?>