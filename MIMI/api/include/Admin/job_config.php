<?PHP
require_once("job.php");
require_once(__DIR__."/../Config.php");

$job = new Job();

//Provide your site name here
//Provide the email address where you want to get notifications
//$job->SetAdminEmail('info@myindmedtech.in');

//Provide your database login details here:
//hostname, user name, password, database name and table name
//note that the script will create the table (for example, fgusers in this case)
//by itself on submitting register.php for the first time
$job->InitDB(
        /*hostname*/ DB_HOST, 
        /*username*/ DB_USERNAME, 
        /*password*/ DB_PASSWORD, 
        /*database name*/ DB_NAME, 
        /*table name*/'jobs');

//For better security. Get a random string from this link: http://tinyurl.com/randstr
// and put it here
$job->SetRandomKey('kuWFmYSXlU01ISd');

?>