<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Satish
 * @link URL Tutorial link
 */
class DbHandler {

    private $conn;
    function __construct() {
        require_once dirname(__FILE__) . '/DbConnect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }
      /**
     * Creating Subscribe
     * @param String $email
     */
    public function Subscribe($email) {
        $response = array();

            // insert query
            if($stmt = $this->conn->prepare("INSERT INTO DownloadLogs (value, type) VALUES (?, 'Subscribe')")) {
              $stmt->bind_param("s", $email);

              $result = $stmt->execute();

              $stmt->close();
            

              // Check for successful insertion
              if ($result) {
                  // User successfully inserted
                  return CREATED_SUCCESSFULLY;
              } else {
                  // Failed to create user
                  return CREATE_FAILED;
              }
            
              return $response;
            }
            else {
              return $this->conn->error;
            }
    }


      /**
     * Creating Get Link
     * @param String $phone
     */
    public function GetLink($phone) {
        $response = array();

            // insert query
            $stmt = $this->conn->prepare("INSERT INTO DownloadLogs (value, type) VALUES ($phone, 'GetLink')");

            $result = $stmt->execute();

            $stmt->close();

            // Check for successful insertion
            if ($result) {
                // User successfully inserted
                return CREATED_SUCCESSFULLY;
            } else {
                // Failed to create user
                return CREATE_FAILED;
            }
        return $response;
    }


    
      /**
     * Get Notification
     * @param 
     */
    public function GetNotification() {
      $response = array();
      $response['rows'] = 0;

      // insert query
      //$sql = "SELECT * FROM notifications where is_enabled = 1 ORDER BY id DESC LIMIT 1";
      $sql = "SELECT * FROM notifications ORDER BY id DESC LIMIT 1";

      $result = mysqli_query($this->conn, $sql);

      if ($result->num_rows > 0) {
        while($row = mysqli_fetch_assoc($result)) {
          $response['rows'] = 1;
          $response['id'] = $row["id"];
          $response['is_enabled'] = $row["is_enabled"];
          $response['start_date'] = $row["start_date"];
          $response['end_date'] = $row["end_date"];
          $response['content'] = $row["content"];
        }
      } else {
        $response['rows'] = 0;
      }
      return $response;
    }


    


}

?>
