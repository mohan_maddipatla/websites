﻿$(document).ready(function () {

    $("#tableAllJobs a").click(function () {
        var jobId = $(this).attr("jobid");
        var postForm = {
            //Fetch form data
            'jobid': jobId
        };
        request = $.ajax({ url: "../api/getjob.php", type: "post", data: postForm, dataType: 'json' });
        request.done(function (response, textStatus, jqXHR) {
            if (response.success == true) {
                $('#modalAddJob').modal('toggle');
                $("#modalAddJob #jobId").val(response.post["id"]);
                $("#modalAddJob #title").val(response.post["title"]);
                $("#modalAddJob #location").val(response.post["location"]);
                $("#modalAddJob #department").val(response.post["department"]);
                $("#modalAddJob #description").val(response.post["description"]);
                $("#modalAddJob #requirements").val(response.post["requirements"]);
                $("#modalAddJob #jobcode").val(response.post["jobcode"]);
                if (response.post["is_enabled"] === "1")
                {
                    $("#modalAddJob #isactive").prop("checked", true);
                }
                else
                {
                    $("#modalAddJob #isactive").prop("checked", false);
                }
                $("#modalAddJob #addJob").html("Update");
                
            }
            else {
                alert("Unable to get job details. Please try again.");
            }
        });
        request.fail(function (jqXHR, textStatus, errorThrown) {
            console.error("The following error occurred: " + textStatus, errorThrown);
        });
        request.always(function () { });

    });

    $('#modalAddJob').on('show.bs.modal', function (e) {
        clearJobModal();
    })
});

function clearJobModal() {
    $("#modalAddJob #addJob").html("Add");
    $("#modalAddJob #jobId").val("");
    $("#modalAddJob #title").val("");
    //$("#modalAddJob #location").val("");
    $("#modalAddJob #location option:selected").prop("selected", false);
    $("#modalAddJob #location option:first").prop("selected", "selected");

    //$("#modalAddJob #department").val("");
    $("#modalAddJob #department option:selected").prop("selected", false);
    $("#modalAddJob #department option:first").prop("selected", "selected");

    $("#modalAddJob #description").val("");
    $("#modalAddJob #requirements").val("");
    $("#modalAddJob #jobcode").val("");
    $("#modalAddJob #isactive").prop("checked", false);
}

$(document).on("click", "#addJob", function (event) {
    var request;
    var isActive = 0;
    if ($('#isactive').prop("checked") == true)
        isActive = 1;

    var postForm = {
        //Fetch form data
        'jobId': $('#jobId').val(),
        'title': $('#title').val(),
        'location': $('#location').val(),
        'department': $('#department').val(),
        'description': $('#description').val(),
        'requirements': $('#requirements').val(),
        'jobcode': $('#jobcode').val(),
        'isactive': isActive
    };

    console.log(postForm);
    request = $.ajax({ url: "../api/postjob.php", type: "post", data: postForm, dataType: 'json' });
    request.done(function (response, textStatus, jqXHR) {
        console.log(response);
        if (response.success == true) {
            $('#modalAddJob').modal('toggle');
            location.reload();
        }
        else {
            alert("Unable to add the job. Please try again.");
        }
    });
    request.fail(function (jqXHR, textStatus, errorThrown) {
        console.error("The following error occurred: " + textStatus, errorThrown);
    });
    request.always(function () { });
});

