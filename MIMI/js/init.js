(function($) {
  
   'use strict';
   
    /* =================================
          PRE LOADER
    =================================== */
    // makes sure the whole site is loaded
    
    $(window).on('load', function() {
        $(".status").fadeOut();
        $(".preloader").delay(1000).fadeOut("slow");
    });
   
   
   $(document).ready(function(){
    /*
    |
    | MIXITUP
    |
    */
        $('#portfolio-item').mixItUp();
        
    /*
    |
    | COLORBOX
    |
    */
        
		
        $('#how-it-works').colorbox({
            iframe: true, width: 640, height: 390, href: function () {
                var videoId = new RegExp('[\\?&]v=([^&#]*)').exec(this.href);
                if (videoId && videoId[1]) {
                    return 'http://youtube.com/embed/' + videoId[1] + '?rel=0&wmode=transparent';
                }
            }
        });

       /*
    |
    | STICKY NAV
    |
    */
        
		    $("#sticker").sticky({topSpacing:0, zIndex: 101});
		
    /*
    |
    | WOW ANIMATION
    |
    */
    	var wow = new WOW({
          mobile: false  // trigger animations on mobile devices (default is true)
      });
      wow.init();
        
    /*
    |
    | SECTION SCROLL
    |
    */
      $('#menuvar').onePageNav({
        currentClass: 'active',
        changeHash: false,
        scrollSpeed: 750,
        scrollThreshold: 0.5,
        filter: ':not(.external)'
      });

      $('a[href*="#"]:not([href="#"])').click(function() {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html, body').animate({
                scrollTop: target.offset().top
                }, 1000);
              return false;
            }
          }
        });

        // Smotth Scroll
        smoothScroll.init();

    /*
    |
    | CONTACT FORM
    |
    */
        $('.mobile-number').keypress(function (e) {
            if (e.charCode > 57 || e.charCode < 48)
                e.preventDefault();
        });

        $('.valid-name').keypress(function (e) {
            console.log(e.charCode);
            //valid 97-122(small), 65-90(caps), 32(space), 39 ('), 
            if ((e.charCode < 97 || e.charCode > 122) && (e.charCode < 65 || e.charCode > 90) && (e.charCode != 32) && (e.charCode != 39))
                e.preventDefault();
        });

        
        $("#contactForm").validator().on("submit", function (event) {
            if (event.isDefaultPrevented()) {
              // handle the invalid form...
              formError();
              //submitMSG(false, "Did you fill in the form properly?");
              submitMSG(false, "Please fill all the details");
          } else {
              // everything looks good!
              event.preventDefault();
              submitForm();
            }
         });
    
        function submitForm() {
            var name = $("#name").val();
            var lname = $("#L_name").val();
            var phone = $("#phone").val();
            var email = $("#email").val();
            var message = $("#message").val();
            $.ajax({
                type: "POST",
                url: "api/contact.php",
                data: "name=" + name + "&lastname=" + lname + "&email=" + email + "&phone=" + phone + "&message=" + message,
                success: function (data) {
                    var retObj = $.parseJSON(data);
                    console.log(retObj);
                    if (retObj.success == true) {
                        formSuccess(retObj.posted);
                    } else {
                        var msg = '';
                        $.each(retObj.errors, function (i, v) {
                            msg += v + "<br>";
                        });

                        formError();
                        submitMSG(false, msg);
                        console.log(retObj.error_logs);
                    }
                    /*
                    if (text == "success"){
                        formSuccess();
                      } else {
                        formError();
                        submitMSG(false,text);
                      }
                      */
                }
            });
        }
        function formSuccess(msg) {
            $("#contactForm")[0].reset();
            submitMSG(true, msg)
        }
    	  function formError(){   
    	    $("#contactForm").removeClass().addClass('shake animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
    	        $(this).removeClass();
    	    });
    	  }
        function submitMSG(valid, msg){
          if(valid){
            var msgClasses = "h3 text-center fadeInUp animated text-success";
          } else {
            var msgClasses = "h3 text-center shake animated text-danger";
          }
          $("#msgSubmit").removeClass().addClass(msgClasses).html(msg);
        }
    });

} (jQuery) );