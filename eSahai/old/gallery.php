﻿<!DOCTYPE html>
<html lang=en>
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta http-equiv=X-UA-Compatible content="IE=edge">
<link rel="shortcut icon" href=favicon.ico type="image/x-icon"/>
<meta name=google-site-verification content=kZd79_ytYQ7rjfWV9IXwal5MfrCQY9cpEb4xSCqWqAo />
<title>eSahai | Gallery</title>
<meta name=description content="eSahai provides 24X7 emergency medical services. Round the clock service is assured by trained staff and equipped vehicles.">
<meta name=viewport content="width=device-width, initial-scale=1">
<meta property=og:type content=website>
<meta property=og:title content="eSahai | 24x7 Availability">
<meta property=og:description content="eSahai provides 24X7 emergency medical services. Round the clock service is assured by trained staff and equipped vehicles.">
<meta property=og:url content="http://www.esahai.in/contact.html">
<meta property=og:image content="http://www.esahai.in/img/home-bg.png">
<meta name=twitter:card content=summary>
<meta name=twitter:site content="@eSahaiIndia">
<meta name=twitter:title content="eSahai | 24x7 Availability">
<meta name=twitter:description content="eSahai provides 24X7 emergency medical services. Round the clock service is assured by trained staff and equipped vehicles.">
<meta name=twitter:image content="http://www.esahai.in/img/home-bg.png">
<meta name=twitter:image:alt content="Trained Staff &amp; Certified Ambulance Vehicles">
<link rel="publisher" href="https://plus.google.com/u/0/104711502073429703353">
<style id=jarallax-clip-0>p{text-align:justify;}</style>
<link rel="stylesheet" type="text/css" href="/css/main.min.css"/>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Slab">	  
</head>
<body data-spy=scroll data-target=.navbar data-offset=50>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<div class=body-wrapper>
<div class=preloader style="display: none;">
<div class=spinner-wrap style="display: none;">
<div class=spinner></div>
<span class="preloader-text alt-font">loading</span>
</div>
</div>
<header>
<nav class="navbar navbar-transparent navbar-fixed-top top-nav-collapse">
<div class=container>
<div class=navbar-header>
<button type=button class="navbar-toggle collapsed" data-toggle=collapse data-target="#navbar" aria-expanded=false aria-controls=navbar>
<span class=sr-only>Toggle navigation</span>
<span class=icon-bar></span>
<span class=icon-bar></span>
<span class=icon-bar></span>
</button>
<a class="navbar-brand logo" href="/"><img src="./img/logo-esahai.png" alt="eSahai Logo"></a>
</div>
<div id=navbar class="navbar-collapse collapse" aria-expanded=false style="height: 1px;" role=navigation>
<ul class="nav navbar-nav navbar-right">
<li style="padding-top:5px;">
<div class=pull-left>
<a href="https://play.google.com/store/apps/details?id=com.esahai&hl=en" title="Get App for Android" target=_blank style="padding-left: 10px;padding-right: 0px;">
<img src="img/Google-Play-Store.png" alt=Android width=32 height=32 style="margin-top: 3px;"/>
</a>
</div>
<div class=pull-left>
<a href="#" onclick="return false" title="IOS App in the making. Please do subscribe to get a link when it is released." data-placement=bottom target=_blank style="padding-left: 10px;padding-right: 0px;">
<img src="img/ios-icon.png" alt=iPhone />
</a>
</div>
<a href=index.html style=visibility:hidden>
Features
</a>
</li>
<li>
<a href="index.html#app-star-feature-section">
Features
</a>
</li>
<li class="">
<a href="index.html#app-star-srceenshot-section">
Screenshot
</a>
</li>
<li class="">
<a href="index.html#advisors-section">
Advisors
</a>
</li>
<li class="">
<a href="index.html#app-star-clients-section">
Partners
</a>
</li>
<li class=active>
<a href=gallery.php>
Gallery
</a>
</li>
<li class="">
<a href=contact.html>
Contact Us
</a>
</li>
<li class="">
<a href="index.html#app-star-footer" title="Download &amp; Subscribe">
Download
</a>
</li>
</ul>
</div>
</div>
</nav>
</header>
<section id=contact_us class="">
<div class=section-title>
<div class=container>
<div class=row>
<div class=col-md-12>
<h2>Gallery</h2>
</div>
</div>
</div>
</div>
<div id=section-features>
<div class=container style="padding-top:50px;">
<div class=row>
<div class=col-md-12>
<?php
try {
$path = 'img/gallery';
if(empty($_GET)) {
$items = array_diff(scandir($path), array('.', '..'));
foreach ($items as $item) {
if(is_dir($path.'/'.$item))
{
$path1 = $path.'/'.$item;
$imgs = array_diff(scandir($path1), array('.', '..'));
if(sizeof($imgs)>0){
echo('<div class=col-md-3><div class="panel panel-default"><div class="panel-body"><a href="gallery.php?fn='.$item.'"><div class="" style="font-size:28px;display: block;text-align: center;"><img class="gImg img-responsive" src="'.$path1.'/'.current($imgs).'"></div><br/><div class="text-center">'.$item.'</div></a></div></div></div>');
}
//else {
//	echo('<div class=col-md-3><div class="panel panel-default"><div class="panel-body"><a href="gallery.php?fn='.$item.'"><div class="" style="font-size:28px;display: block;text-align: center;"></div><br/><div class="text-center">'.$item.'</div></a></div></div></div>');					
//}
}
}
}
else {
echo('<div class="pull-right"><a href="gallery.php"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back to Gallery </a></div><br/><br/>');
echo("<div id='div-imgs'>");
if(isset($_GET["fn"])) {
$folderName = trim($_GET["fn"]);
echo("<h3>$folderName</h3><br/>");
$path = $path.'/'.$folderName;
$items = array_diff(scandir($path), array('.', '..'));
foreach ($items as $item) {
echo('<div class="col-md-3">');
echo('<div class="" style="padding-bottom:20px;"><a href="'.$path.'/'.$item.'"><img class="gImg img-responsive" src="'.$path.'/'.$item.'"></a></div>');
echo('</div>');
}
}
else {
            
}
echo('</div>');
}
}
catch(Exception $e) {
echo('');
}
?>
<br/><br/><br/><br/><br/><br/>
</div>
</div>
</div>
</div>
</section>
<div class=footer-terms>
<div class=container>
<div class=pull-left>
<a href="http://www.twitter.com/esahaiindia" target=_blank><i class="fa fa-twitter"></i></a>
<a href="http://www.facebook.com/esahaiindia" target=_blank><i class="fa fa-facebook"></i></a>
<a href="http://www.instagram.com/esahaiindia" target=_blank><i class="fa fa-instagram"></i></a>
</div>
<a class=pull-right href=disclaimer.html>Disclaimer</a>
<a class=pull-right href=terms_of_use.html>Terms of Use</a>
<a class=pull-right href=privacy_policy.html>Privacy Policy</a>
</div>
</div>
</div>
<div itemprop itemscope itemtype="http://schema.org/Service"> <meta itemprop="name" content="Reliable Round The Clock Emergency Medical Services" /> <meta itemprop="description" content="Our professionally trained paramedic staff along with periodically checked and well maintained ambulance vehicles are round the clock ready to attend any type of medical emergencies, just on a click from mobile." /> <meta itemprop="provider" content="eSahai.in" /> <meta itemprop="serviceType" content="Emergency Medical Services" /> <meta itemprop="serviceArea" content="Hyderabad" /></div>
<script type="application/ld+json"> { "@graph": [ { "@context": "http://schema.org", "@type": "WebSite", "url": "http://esahai.in", "potentialAction": { "@type": "SearchAction", "target": "http://query.esahai.in/search?q={search_term_string}", "query-input": "required name=search_term_string" } } , { "@context": "http://schema.org", "@type": "Organization", "name": "eSahai", "url": "http://esahai.in", "sameAs": [ "https://www.facebook.com/esahaiindia", "https://twitter.com/esahaiindia"] } ]}</script><script type="application/ld+json">{"@context":"http://schema.org/","@type":"Review","itemReviewed":{"@type":"Organization","image":"http://www.esahai.in/img/home-bg.png","name":"eSahai Emergency Medical Services"},"reviewRating":{"@type":"Rating","ratingValue":"4.9"},"name":"Emergency Medical Transport Services","author":{"@type":"Person","name":"Kiran"},"reviewBody":"Excellent, Convenient and Dependable emergency medical transport and travel services.","publisher":{"@type":"Organization","name":"IT Industry"}}</script>
<div id=sc-icons class=sc-icons></div>
<script src="js/modernizr-2.8.3-respond-1.4.2.min.js"></script>
<script src="js/consolidated.js"></script>
<script>$(document).ready(function(){$("body").on("contextmenu",function(e){return!1}),$("#div-imgs a").colorbox({rel:"gImg",slideshow:!0,slideshowSpeed:"2500",maxWidth:"100%",maxHeight:"100%"})});</script>
</body>
</html>