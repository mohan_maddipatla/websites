<?PHP
require_once("../api/include/Admin/appsettings_config.php");
require_once("../api/include/Admin/member_config.php");

if(!$member->CheckLogin())
{
    $member->RedirectToURL("login.php");
    exit;
}

if(isset($_POST['submitted']))
{
   if($appsettings->SubmitNotification())
   {
   }
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>eSahai Settings</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Google Fonts -->
    <link href="../css/css.css" rel="stylesheet">
    <!-- Stylesheets -->
    <link rel="stylesheet" href="../css/font-awesome.min.css">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <!-- jquery UI -->
    <link rel="stylesheet" href="../css/jquery-ui.min.css">

    <!-- colorbox CSS-->
    <link rel="stylesheet" href="../css/colorbox.css">
    <!-- toastr CSS-->
    <link rel="stylesheet" href="../css/toastr.min.css" />        
    <!-- main css -->
    <link rel="stylesheet" href="../css/main.css">
    <!-- roboto font -->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab" rel="stylesheet">
    <!-- modernizr -->
    <script src="../js/analytics.js"></script>
    <script src="../js/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <style type="text/css" id="jarallax-clip-0">
        #jarallax-container-0 {
            clip: rect(0px 1349px 1221px 0);
            clip: rect(0px, 1349px, 1221px, 0);
        }
		.section-title{
			min-height:200px;
			padding-top: 0px!important;
		}
		.active a {
			color: #eda220 !important;
			font-weight: bold;
		}
    </style>
</head>
<body data-spy="scroll" data-target=".navbar" data-offset="50">
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <!-- body wrapper START -->
    <div class="body-wrapper">


    </div>
    <!-- body wrapper ENDs -->
        <!-- preloader START -->
        <div class="preloader" style="display: none;">
            <div class="spinner-wrap" style="display: none;">
                <div class="spinner"></div>
                <span class="preloader-text alt-font">loading</span>
            </div>
        </div>
        <!-- preloader END -->
        <!-- =========================
            page header START
        ============================== -->
        <header>
            <!-- main navigation START -->
            <nav class="navbar navbar-inverse navbar-fixed-top top-nav-collapse">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand logo" href="../index.html"><img src="../img/logo-esahai.png" alt="eSahai"></a>
                    </div>
                    <!-- Navbar -->
                    <div id="navbar" class="navbar-collapse collapse" aria-expanded="false" style="height: 1px;">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="active">
                                <a href="settings.php">
                                    Settings
                                </a>
                            </li>
                            <li class="">
                                <a href="profile.php">
                                    Profile
                                </a>
                            </li>
                            <li class="">
                                <a href="change_pwd.php">
                                    Change Password
                                </a>
                            </li>
                            <li class="">
                                <a href="logout.php">
                                    Logout
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!--/.navbar-collapse -->
                </div>
            </nav>
            <!-- main navigation END -->
        </header>
        <!-- page header END -->
        <!-- =========================
            Login
        ============================== -->
        <section id="emergency-medical" class="app-star-feature-details-section">
            <div class="section-title">
                <div class="container">
                    <div class="row">
                        <!-- main title -->
                        <div class="col-md-12">
                            <h2>App Settings</h2>
                            <!-- <img src="./img/title_line.png" alt=""> -->
                        </div>
                    </div>
                </div>
            </div>
            <div id="section-features">
                <!-- feature AREA START HERE -->
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
							<!-- Form Code Start -->
							<div id='settingsDiv'>
								<form id='settings' class='form' action='<?php echo $appsettings->GetSelfScript(); ?>' method='post' accept-charset='UTF-8'>
									<input type='hidden' name='submitted' id='submitted' value='1'/>
									<input type='hidden' name='notificationId' id='notificationId' value='0'/>
									<div class="panel panel-default">
										<div class="panel-heading">Notification Settings</div>
										<div class="panel-body">
											<div class="row">
												<div class="col-md-2">Enable Notification</div>
												<div class="col-md-4">
													<input id='cbEnableNotification' name='cbEnableNotification' type="checkbox" aria-label="">
												</div>
											</div>
											<div class="row">
												<div class="col-md-2">Start Date</div>
												<div class="col-md-4">
													<input type="text" id="notificationStartDate" name="notificationStartDate" class="date" value='<?php echo $appsettings->SafeDisplay('notificationStartDate') ?>'>
												</div>
												<div class="col-md-2">End Date</div>
												<div class="col-md-4">
													<input type="text" id="notificationEndDate" name="notificationEndDate" class="date" value='<?php echo $appsettings->SafeDisplay('notificationEndDate') ?>'>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12">Content: </div>
											</div>
											<div class="row">
												<div class="col-md-12"><textarea id='taNotification' name='taNotification' rows="7"><?php echo $appsettings->SafeDisplay('taNotification') ?></textarea></div>
											</div>
											<div class="row">
												<div class="col-md-12">
													<input class="btn btn-primary pull-right" type="submit" name="submit" value="submit">
												</div>
											</div>
											<?php echo $appsettings->error_message ?>
									  </div>
									</div>
								</form>
							</div>
                        </div>
                    </div>
                    <!-- end row -->
                </div>
                <!-- end container -->
            </div>
        </section>
        <!-- End Emergency medical services -->



	<script type='text/javascript' src='../js/gen_validatorv4.js'></script>

    <!-- jquery library -->
    <script src="../js/jquery-1.11.2.min.js"></script>
    <!-- Bootstrap  -->
    <script src="../js/bootstrap.min.js"></script>
    <!-- jquery UI -->
    <script src="../js/jquery-ui.min.js"></script>

    <!-- contact js -->
    <script src="../js/jquery-contact.js"></script>
    <!-- retina js -->
    <script src="../js/retina.min.js"></script>
    <!-- lightbox
    <script src="../js/nivo-lightbox.min.js"></script>
        -->
    <!-- fancybox JS
    <script src="../js/jquery.fancybox.pack.js"></script>
        -->
    <!-- colorbox JS-->
    <script src="../js/jquery.colorbox-min.js"></script>

    <!-- toastr JS-->
    <script src="../js/toastr.min.js"></script>

    <!-- mailchimp -->
    <script src="../js/jquery.ajaxchimp.min.js"></script>
    <!-- scroll animatin JS -->
    <script src="../js/scrollreveal.min.js"></script>
	<!-- tinymce -->
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <!-- Custom js -->
    <script src="../js/main.js"></script>
	<script type="text/javascript">
	  $.get("../api/notifications.php", function (data, status) {
		  if (status == 'success') {
			  var jsonobj = $.parseJSON(data);
			  if(jsonobj.success == true) {
				  if (jsonobj.posted.rows > 0) {
					$('#notificationId').val(jsonobj.posted.id);
					if (jsonobj.posted.is_enabled == "1") {
						$( "#cbEnableNotification" ).prop( "checked", true );
					}
					else {
						$( "#cbEnableNotification" ).prop( "checked", false );
					}
					$('#notificationStartDate').val(jsonobj.posted.start_date);
					$('#notificationEndDate').val(jsonobj.posted.end_date);
					//$('#taNotification').html(jsonobj.posted.content);
					$(tinymce.get('taNotification').getBody()).html(jsonobj.posted.content);
				  }
			  }
			  else
			  {
				alert("Unable to retrieve data. Please try again later.");
				console.log(jsonobj);
			  }
		  }
	  });


	  tinymce.init({
		selector: '#taNotification',
		menubar: false,
		image_title: true, 
		automatic_uploads: true,
		images_upload_url: 'imageHandler.php',
		plugins: "image",
		file_browser_callback_types: 'image',
		file_picker_types: 'image',
		file_picker_callback: function(cb, value, meta) {
			var input = document.createElement('input');
			input.setAttribute('type', 'file');
			input.setAttribute('accept', 'image/*');
    
			// Note: In modern browsers input[type="file"] is functional without 
			// even adding it to the DOM, but that might not be the case in some older
			// or quirky browsers like IE, so you might want to add it to the DOM
			// just in case, and visually hide it. And do not forget do remove it
			// once you do not need it anymore.

			input.onchange = function() {
			  var file = this.files[0];
			  // Note: Now we need to register the blob in TinyMCEs image blob
			  // registry. In the next release this part hopefully won't be
			  // necessary, as we are looking to handle it internally.
			  var id = 'blobid' + (new Date()).getTime();
			  var blobCache = tinymce.activeEditor.editorUpload.blobCache;
			  var blobInfo = blobCache.create(id, file);
			  blobCache.add(blobInfo);
      
			  // call the callback and populate the Title field with the file name
			  cb(blobInfo.blobUri(), { title: file.name });
			};
    
			input.click();
		  }
	  });

	  //$(".date").datepicker({ dateFormat : 'dd-mm-yy' });


	$( function() {
		var dateFormat = "dd/mm/yy",
			from = $( "#notificationStartDate" )
				.datepicker({
				dateFormat:"dd/mm/yy"
				})
				.on( "change", function() {
					to.datepicker( "option", "minDate", getDate( this ) );
				}),
			to = $( "#notificationEndDate" )
			.datepicker({
				dateFormat:"dd/mm/yy"
			})
			.on( "change", function() {
				from.datepicker( "option", "maxDate", getDate( this ) );
			});

		function getDate( element ) {
			var date;
			try {
				date = $.datepicker.parseDate( dateFormat, element.value );
			} catch( error ) {
				date = null;
			}

			return date;
		}
	} );
	
  </script>

</body>
</html>