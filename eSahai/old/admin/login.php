<?PHP
require_once("../api/include/Admin/member_config.php");

if(isset($_POST['submitted']))
{
   if($member->Login())
   {
        $member->RedirectToURL("settings.php");
   }
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>eSahai Admin Login</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Google Fonts -->
    <link href="../css/css.css" rel="stylesheet">
    <!-- <link href="../css/css(1).css" rel="stylesheet">-->
    <!-- Stylesheets -->
    <link rel="stylesheet" href="../css/font-awesome.min.css">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <!-- Carousel
    <link rel="stylesheet" href="../css/carousel.css">
        -->
    <!-- nivo lightbox 
    <link rel="stylesheet" href="../css/nivo-lightbox.css">
    <link rel="stylesheet" href="../css/default.css">
        -->
    <!-- fancybox CSS 
    <link rel="stylesheet" href="../css/jquery.fancybox.css">
        -->
    <!-- colorbox CSS-->
    <link rel="stylesheet" href="../css/colorbox.css">
    <!-- toastr CSS-->
    <link rel="stylesheet" href="../css/toastr.min.css" />        
    <!-- main css -->
    <link rel="stylesheet" href="../css/main.css">
    <!-- roboto font -->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab" rel="stylesheet">
    <!-- modernizr -->
    <script src="../js/analytics.js"></script>
    <script src="../js/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <style type="text/css" id="jarallax-clip-0">
        #jarallax-container-0 {
            clip: rect(0px 1349px 1221px 0);
            clip: rect(0px, 1349px, 1221px, 0);
        }
		.section-title{
			min-height:200px;
			padding-top: 0px!important;
		}
		.btn-primary {
			background-color: #337ab7;
		}
    </style>
</head>
<body data-spy="scroll" data-target=".navbar" data-offset="50">
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <!-- body wrapper START -->
    <div class="body-wrapper">


    </div>
    <!-- body wrapper ENDs -->
        <!-- preloader START -->
        <div class="preloader" style="display: none;">
            <div class="spinner-wrap" style="display: none;">
                <div class="spinner"></div>
                <span class="preloader-text alt-font">loading</span>
            </div>
        </div>
        <!-- preloader END -->
        <!-- =========================
            page header START
        ============================== -->
        <header>
            <!-- main navigation START -->
            <nav class="navbar navbar-inverse navbar-fixed-top top-nav-collapse">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand logo" href="/"><img src="../img/logo-esahai.png" alt="eSahai"></a>
                    </div>
                    <!-- Navbar -->
                    <div id="navbar" class="navbar-collapse collapse" aria-expanded="false" style="height: 1px;">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                            </li>
                        </ul>
                    </div>
                    <!--/.navbar-collapse -->
                </div>
            </nav>
            <!-- main navigation END -->
        </header>
        <!-- page header END -->
        <!-- =========================
            Login
        ============================== -->
        <section id="emergency-medical" class="app-star-feature-details-section">
            <div class="section-title">
                <div class="container">
                    <div class="row">
                        <!-- main title -->
                        <div class="col-md-12">
                            <h2>Login</h2>
                            <!-- <img src="./img/title_line.png" alt=""> -->
                        </div>
                    </div>
                </div>
            </div>
            <div id="section-features">
                <!-- feature AREA START HERE -->
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
							<!-- Form Code Start -->
							<div id='membersite' style='centered'>
								<form id='login' class='form' action='<?php echo $member->GetSelfScript(); ?>' method='post' accept-charset='UTF-8'>
									<fieldset>
										<input type='hidden' name='submitted' id='submitted' value='1'/>
										<div class='col-md-12 loginrow'>
											<div class="input-group">
											  <span class="input-group-addon fa fa-user" id="basic-addon1"></span>
											  <input name='username' id='username' value='<?php echo $member->SafeDisplay('username') ?>' maxlength="50" type="text" class="form-control" placeholder="Username" aria-describedby="basic-addon1">
											</div>
											<div class='loginError'>
												<span id='login_username_errorloc' class='error'></span>
											</div>
										</div>
										<div class='col-md-12 loginrow'>
											<div class="input-group">
											  <span class="input-group-addon fa fa-lock" id="basic-addon1"></span>
											  <input name='password' id='password' maxlength="50" type="password" class="form-control" placeholder="Password" aria-describedby="basic-addon1">
											</div>
											<div class='loginError'>
											  <span id='login_password_errorloc' class='error'></span>
											</div>
										</div>
										<div class='col-md-12 loginrow'>
											<input type='submit' name='Submit' value='Submit' class='btn btn-primary btn-block' />
											<a id="aForgotPassword" class="pull-right" href="#" style="font-size: 12px;">Forgot Password</a>
										</div>
										<div class="col-md-12 loginrow"></div>
										<div><span class='error'><?php echo $member->GetErrorMessage(); ?></span></div>
									</fieldset>
								</form>
                        </div>
                    </div>
                    <!-- end row -->
                </div>
                <!-- end container -->
            </div>
        </section>
        <!-- End Emergency medical services -->



	<script type='text/javascript' src='../js/gen_validatorv4.js'></script>

    <!-- jquery library -->
    <script src="../js/jquery-1.11.2.min.js"></script>
    <!-- Bootstrap  -->
    <script src="../js/bootstrap.min.js"></script>
    <!-- Toastr js -->
    <script src="../js/toastr.min.js"></script>
    <!-- contact js -->
    <script src="../js/jquery-contact.js"></script>
    <!-- retina js -->
    <script src="../js/retina.min.js"></script>
    <!-- lightbox
    <script src="../js/nivo-lightbox.min.js"></script>
        -->
    <!-- fancybox JS
    <script src="../js/jquery.fancybox.pack.js"></script>
        -->
    <!-- colorbox JS-->
    <script src="../js/jquery.colorbox-min.js"></script>

    <!-- mailchimp -->
    <script src="../js/jquery.ajaxchimp.min.js"></script>
    <!-- scroll animatin JS -->
    <script src="../js/scrollreveal.min.js"></script>
    <!-- Custom js -->
    <script src="../js/main.js"></script>

<script>
    $("#aForgotPassword").click(function (event) {
        event.preventDefault();
        var postForm = {
            'username': $('#username').val()
        };
        request = $.ajax({ url: "reset_pwd.php", type: "post", data: postForm, dataType: 'json' });
        request.done(function (response, textStatus, jqXHR) {
            if (response.success == true) {
                alert("Your password has been reset and sent to your registered email.");
                //showAlert("Your password has been reset and sent to your registered email.");
            }
            else {
                alert(response.errors);
                //alert(response.errors);
				console.log(response);
            }
        });
        request.fail(function (jqXHR, textStatus, errorThrown) {
            console.error("The following error occurred: " + textStatus, errorThrown);
            //showAlert("There is a problem in processing your request. Please try again later.");
        });
    });


</script>
<!-- client-side Form Validations:
Uses the excellent form validation script from JavaScript-coder.com-->

<script type='text/javascript'>
// <![CDATA[

    var frmvalidator  = new Validator("login");
    frmvalidator.EnableOnPageErrorDisplay();
    frmvalidator.EnableMsgsTogether();

    frmvalidator.addValidation("username","req","Please provide your username");
    
    frmvalidator.addValidation("password","req","Please provide the password");

// ]]>
</script>
<!--
Form Code End (see html-form-guide.com for more info.)
-->

</body>
</html>