<?PHP
require_once("../api/include/Admin/member_config.php");

if(!$member->CheckLogin())
{
    $member->RedirectToURL("login.php");
    exit;
}

if(isset($_POST['submitted']))
{
   if($member->ChangePassword())
   {
        $member->RedirectToURL("settings.php");
   }
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Change Password</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Google Fonts -->
    <link href="../css/css.css" rel="stylesheet">
    <!-- Stylesheets -->
    <link rel="stylesheet" href="../css/font-awesome.min.css">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <!-- jquery UI -->
    <link rel="stylesheet" href="../css/jquery-ui.min.css">

    <!-- colorbox CSS-->
    <link rel="stylesheet" href="../css/colorbox.css">
    <!-- toastr CSS-->
    <link rel="stylesheet" href="../css/toastr.min.css" />        
    <!-- main css -->
    <link rel="stylesheet" href="../css/main.css">
    <!-- roboto font -->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab" rel="stylesheet">
    <!-- modernizr -->
    <script src="../js/analytics.js"></script>
    <script src="../js/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <style type="text/css" id="jarallax-clip-0">
        #jarallax-container-0 {
            clip: rect(0px 1349px 1221px 0);
            clip: rect(0px, 1349px, 1221px, 0);
        }
		.section-title{
			min-height:200px;
			padding-top: 0px!important;
		}
		.btn-primary {
			background-color: #337ab7;
		}
		.active a {
			color: #eda220 !important;
			font-weight: bold;
		}
    </style>
</head>
<body data-spy="scroll" data-target=".navbar" data-offset="50">
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <!-- body wrapper START -->
    <div class="body-wrapper">


    </div>
    <!-- body wrapper ENDs -->
        <!-- preloader START -->
        <div class="preloader" style="display: none;">
            <div class="spinner-wrap" style="display: none;">
                <div class="spinner"></div>
                <span class="preloader-text alt-font">loading</span>
            </div>
        </div>
        <!-- preloader END -->
        <!-- =========================
            page header START
        ============================== -->
        <header>
            <!-- main navigation START -->
            <nav class="navbar navbar-inverse navbar-fixed-top top-nav-collapse">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand logo" href="../index.html"><img src="../img/logo-esahai.png" alt="eSahai"></a>
                    </div>
                    <!-- Navbar -->
                    <div id="navbar" class="navbar-collapse collapse" aria-expanded="false" style="height: 1px;">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="settings.php">
                                    Settings
                                </a>
                            </li>
                            <li class="">
                                <a href="profile.php">
                                    Profile
                                </a>
                            </li>
                            <li class="active">
                                <a href="#">
                                    Change Password
                                </a>
                            </li>
                            <li class="">
                                <a href="logout.php">
                                    Logout
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!--/.navbar-collapse -->
                </div>
            </nav>
            <!-- main navigation END -->
        </header>
        <!-- page header END -->
        <!-- =========================
            Change Password
        ============================== -->
        <section id="emergency-medical" class="app-star-feature-details-section">
            <div class="section-title">
                <div class="container">
                    <div class="row">
                        <!-- main title -->
                        <div class="col-md-12">
                            <h2>Change Password</h2>
                            <!-- <img src="./img/title_line.png" alt=""> -->
                        </div>
                    </div>
                </div>
            </div>
            <div id="section-features">
                <!-- feature AREA START HERE -->
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
							<!-- Form Code Start -->
							<div id='membersite'>
								<form id='changepwd' action='<?php echo $member->GetSelfScript(); ?>' method='post' accept-charset='UTF-8'>
								<fieldset>
									<input type='hidden' name='submitted' id='submitted' value='1'/>
									<div class='col-md-12 loginrow'>
										<div class="">
											<input name='oldpwd' id='oldpwd' maxlength="50" type="password" class="form-control" placeholder="Old Password" aria-describedby="basic-addon1">
										</div>
										<div class='loginError'>
											<span id='changepwd_oldpwd_errorloc' class='error'></span>
										</div>
									</div>
									<div class='col-md-12 loginrow'>
										<div class="">
											<input name='newpwd' id='newpwd' maxlength="50" type="password" class="form-control" placeholder="New Password" aria-describedby="basic-addon1">
										</div>
										<div class='loginError'>
											<span id='changepwd_newpwd_errorloc' class='error'></span>
										</div>
									</div>
									<div class='col-md-12 loginrow'>
										<input type='submit' name='Submit' value='Submit' class='btn btn-primary btn-block' />
									</div>
									<div class='col-md-12 loginrow'>
										<a href="settings.php" class='btn btn-danger btn-block'>Cancel</a>
									</div>
									<div><span class='error'><?php echo $member->GetErrorMessage(); ?></span></div>
								</fieldset>
								</form>

							</div>
                        </div>
                    </div>
                    <!-- end row -->
                </div>
                <!-- end container -->
            </div>
        </section>
        <!-- End Emergency medical services -->

    <!-- jquery library -->
    <script src="../js/jquery-1.11.2.min.js"></script>
    <!-- Bootstrap  -->
    <script src="../js/bootstrap.min.js"></script>
    <!-- jquery UI -->
    <script src="../js/jquery-ui.min.js"></script>

    <!-- contact js -->
    <script src="../js/jquery-contact.js"></script>
    <!-- retina js -->
    <script src="../js/retina.min.js"></script>
    <!-- lightbox
    <script src="../js/nivo-lightbox.min.js"></script>
        -->
    <!-- fancybox JS
    <script src="../js/jquery.fancybox.pack.js"></script>
        -->
    <!-- colorbox JS-->
    <script src="../js/jquery.colorbox-min.js"></script>

    <!-- mailchimp -->
    <script src="../js/jquery.ajaxchimp.min.js"></script>
    <!-- scroll animatin JS -->
    <script src="../js/scrollreveal.min.js"></script>
	<!-- tinymce -->
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <!-- Custom js -->
    <script src="../js/main.js"></script>

	<!-- client-side Form Validations:
	Uses the excellent form validation script from JavaScript-coder.com-->

</body>
</html>