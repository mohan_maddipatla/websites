var showToastr = true;
if (sessionStorage.getItem('shTstr') === null) {
    sessionStorage.setItem('shTstr', 'true');
} else {
    if (sessionStorage.getItem('shTstr') == 'false') {
        showToastr = false;
    }
}

(function($) {
    "use strict";

    try {
        var vUrl = "";
        $.getJSON("video.json", function (data) {
            vUrl = data.videoUrl;
        });

        /*$('#how-it-works').colorbox({
            iframe: true, width: 640, height: 390, href: function () {
                //var videoId = new RegExp('[\\?&]v=([^&#]*)').exec(this.href);
                var videoId = new RegExp('[\\?&]v=([^&#]*)').exec(vUrl);
                if (videoId && videoId[1]) {
                    return 'http://youtube.com/embed/' + videoId[1] + '?rel=0&wmode=transparent&autoplay=1&controls=0';
                }
            }
        });
        */
        $('#how-it-works').colorbox({
            inline: true, width: 640, height: 390, href: '#promovideo'
        });
    }
    catch (ex) { }
	/* ==============================================
     Screenshot carousel  -->
     =============================================== */

    // Instantiate the Bootstrap carousel
      $('.multi-item-carousel').carousel({
          interval: false
      });

    // for every slide in carousel, copy the next slide's item in the slide.
    // Do the same for the next, next item.
      $('.multi-item-carousel .item').each(function () {
          var next = $(this).next();
          if (!next.length) {
              next = $(this).siblings(':first');
          }
          next.children(':first-child').clone().appendTo($(this));

          for (var i = 0; i < 2; i++) {
              next = next.next();
              if (!next.length) {
                  next = $(this).siblings(':first');
              }
              next.children(':first-child').clone().appendTo($(this));
          }

          /*
          if (next.next().length > 0) {
              next.next().children(':first-child').clone().appendTo($(this));
          } else {
              $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
          }
          */
      });

      function initCarouselIndicators() {
          $(".carousel-indicators[data-target]").each(function (i, indicators) {
              var targetId = indicators.dataset.target;
              if (targetId != "") {
                  var $carousel = $(targetId);
                  $carousel.bind('slide.bs.carousel', function (e) {
                      var $targetSlide = $(e.relatedTarget);
                      var index = $targetSlide.index();
                      $('.carousel-indicators[data-target="' + targetId + '"] li').removeClass('active')
                      $('.carousel-indicators[data-target="' + targetId + '"] li:nth-child(' + (index + 1) + ')').addClass('active');
                  });
              }
          });
      }

      //initCarouselIndicators();

	/* ==============================================
     Nivo Lightbox  -->
     =============================================== */
    /*
    $('#screenshots a').nivoLightbox({
        effect: 'fadeScale'

	 });
     */

    /* ==============================================
     Target menu section  -->
     =============================================== */

  $('.btn-wrap').find('a[href*=#]:not([href=#])').on('click', function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: (target.offset().top - 0)
                }, 1000);
                if ($('.navbar-toggle').css('display') != 'none') {
                    $(this).parents('.container').find(".navbar-toggle").trigger("click");
                }
                return false;
            }
        }     
    });


  $(".get-play-store").click(function () {
      // Prevent default posting of form - put here to work in case of errors
      //event.preventDefault();
  });
  $(".get-app-store").click(function () {
      // Prevent default posting of form - put here to work in case of errors
      event.preventDefault();
  });

    /* ==============================================
     Subscribe and Get Link -->
     =============================================== */
  var request;

  $("#subscribe-form").submit(function (event) {
      // Prevent default posting of form - put here to work in case of errors
      event.preventDefault();

      // Abort any pending request
      if (request) {
          request.abort();
      }
      // setup some local variables
      var $form = $(this);

      // Let's select and cache all the fields
      var $inputs = $form.find("input, select, button, textarea");

      var postForm = {
          //Fetch form data
          'email': $('#subscribe-email').val() //Store name fields value
      };

      // Let's disable the inputs for the duration of the Ajax request.
      // Note: we disable elements AFTER the form data has been serialized.
      // Disabled form elements will not be serialized.
      $inputs.prop("disabled", true);

      // Fire off the request to /form.php
      request = $.ajax({ url: "api/subscribe.php", type: "post", data: postForm, dataType  : 'json' });

      // Callback handler that will be called on success
      request.done(function (response, textStatus, jqXHR) {
          if (response.success == true) {
              showAlert(response.posted);
          }
          else
          {
              showAlert(response.errors);
          }
      });

      // Callback handler that will be called on failure
      request.fail(function (jqXHR, textStatus, errorThrown) {
          console.error("The following error occurred: " + textStatus, errorThrown);
          showAlert("There is a problem in processing your request. Please try again later.");
      });

      // Callback handler that will be called regardless
      // if the request failed or succeeded
      request.always(function () { $inputs.prop("disabled", false); });

      //$inputs.prop("disabled", false);
  });

  $('.mobile-number').keypress(function (e) {
      if (e.charCode > 57 || e.charCode < 48)
          e.preventDefault();
  });
  $('.valid-name').keypress(function (e) {
      //valid 97-122(small), 65-90(caps), 32(space), 39 ('), 
      if ((e.charCode < 97 || e.charCode > 122) && (e.charCode < 65 || e.charCode > 90) && (e.charCode != 32) && (e.charCode != 39))
          e.preventDefault();
  });

  $("#getapplinkForm").click(function (event) {
	  var message = "Click here to download eSahai Android app. https://play.google.com/store/apps/details?id=com.esahai";
	  var smsAlert = "An SMS has been sent to "+$('#getlink-phone').val()+".  Please check.";
	  var url = "http://api.smscountry.com/SMSCwebservice_bulk.aspx?User=MyInd&passwd=Password1&mobilenumber=91"+$('#getlink-phone').val()+"&message="+message+"&sid=mohan&mtype=N&DR=Y";
	  var myWindow = window.open(url, "myWindow", 'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=0,height=0');
	  
	  setTimeout(function() { 
		  myWindow.close();
		  showAlert(smsAlert);		  
	  }, 200);
	  


	  //myWindow.close();
	  
	  /*
      // Prevent default posting of form - put here to work in case of errors
      event.preventDefault();

      var postForm = {
          //Fetch form data
          'phone': $('#getlink-phone').val() //Store name fields value
      };

      // Let's disable the inputs for the duration of the Ajax request.
      // Note: we disable elements AFTER the form data has been serialized.
      // Disabled form elements will not be serialized.
      //$inputs.prop("disabled", true);

      // Fire off the request to /form.php
      request = $.ajax({ url: "api/getlink.php", type: "post", data: postForm, dataType: 'json' });

      // Callback handler that will be called on success
      request.done(function (response, textStatus, jqXHR) {
          if (response.success == true) {
              showAlert(response.posted);
          }
          else {
              showAlert(response.errors);
          }
          $('#getlink-phone').val('');
      });

      // Callback handler that will be called on failure
      request.fail(function (jqXHR, textStatus, errorThrown) {
          console.log(jqXHR);
          console.error("The following error occurred: " + textStatus, errorThrown);
          showAlert("There is a problem in processing your request. Please try again later.");
      });

      // Callback handler that will be called regardless
      // if the request failed or succeeded
      request.always(function () {
          //$inputs.prop("disabled", false);
      });

      //$inputs.prop("disabled", false);
      */
  });


  function showAlert(s) {
      $("#alertmessage").html(s);
      $("#alertmodal").modal();
  }

    /* ==============================================
    Notifications  -->
    =============================================== */
      toastr.options = {
          "closeButton": true,
          "debug": false,
          "newestOnTop": false,
          "progressBar": false,
          "positionClass": "toast-bottom-left",
          "preventDuplicates": false,
          "showDuration": "0",
          "hideDuration": "0",
          "timeOut": "0",
          "extendedTimeOut": "0",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut",
          "tapToDismiss": false,
          "closeOnHover": false,
          "onclick": function () {
              $('#addmodal').modal();
          },
          "onCloseClick": function () {
              showToastr = false;
          }
      }
      if (showToastr) {
          $.get("api/notifications.php", function (data, status) {
              if (status == 'success') {
                  try {
                      var jsonobj = $.parseJSON(data);
                      if (jsonobj.posted.rows > 0) {
                          if (jsonobj.success == true) {
                              if (jsonobj.posted.rows > 0) {
                                  if (jsonobj.posted.is_enabled == "1" && jsonobj.posted.showAdd == true) {
                                      toastr.success(jsonobj.posted.content);
                                      $("#addmodal .modal-content").html(jsonobj.posted.content);
                                      $("#addmodal .modal-content img").removeAttr("height");
                                      $("#addmodal .modal-content img").removeAttr("width");
                                      $("#addmodal .modal-content img").addClass("img-responsive");
                                      $("#addmodal .modal-content img").addClass("center-block");
                                  }
                              }
                          }
                      }
                  }
                  catch (err) { }
              }
          });
      }
     /* ==============================================
     scorll animation  -->
     =============================================== */

      window.sr = ScrollReveal();

      sr.reveal('.reveal-bottom', {
        origin: 'bottom',
        distance: '20px',
        duration: 800,
        delay: 400,
        opacity: 1,
        scale: 0,
        easing: 'linear',
        reset: true
      });

      sr.reveal('.reveal-top', {
        origin: 'top',
        distance: '20px',
        duration: 800,
        delay: 400,
        opacity: 1,
        scale: 0,
        easing: 'linear',
        reset: true
      });

      sr.reveal('.reveal-left', {
        origin: 'left',
        distance: '20px',
        duration: 800,
        delay: 400,
        opacity: 1,
        scale: 0,
        easing: 'linear',
        reset: true
      });

      sr.reveal('.reveal-right', {
        origin: 'right',
        distance: '20px',
        duration: 800,
        delay: 400,
        opacity: 1,
        scale: 0,
        easing: 'linear',
        reset: true
      });
        /*
      sr.reveal('.reveal-left-fade', {
        origin: 'left',
        distance: '20px',
        duration: 800,
        delay: 0,
        opacity: 0,
        scale: 0,
        easing: 'linear',
        mobile: false
      });
      sr.reveal('.reveal-right-fade', {
        origin: 'right',
        distance: '20px',
        duration: 800,
        delay: 0,
        opacity: 0,
        scale: 0,
        easing: 'linear',
        mobile: false
      });

      sr.reveal('.reveal-right-delay', {
        origin: 'right',
        distance: '20px',
        duration: 1000,
        delay: 0,
        opacity: 0,
        scale: 0,
        easing: 'linear',
        mobile: false
      }, 500);
        
      sr.reveal('.reveal-bottom-fade', {
        origin: 'bottom',
        distance: '20px',
        duration: 800,
        delay: 0,
        opacity: 0,
        scale: 0,
        easing: 'linear',
        mobile: false
      });
      */

})(window.jQuery);