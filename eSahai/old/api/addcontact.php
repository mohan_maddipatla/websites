<?php
  require_once 'include/Exceptions.php';
  require_once 'Constants.php';
  require_once 'include/Admin/surveycontact_config.php';
  
	$errors = array(); //To store errors
  $form_data = array(); //Pass back the data to
    
    /* Validate the form on server side */
    if (empty($_POST['name'])) { 
		//Name cannot be empty
        $errors['msg'] = "Please fill in name";
    }
    else if (empty($_POST['email']) && empty($_POST['phone'])) { 
		//Name cannot be empty
        $errors['msg'] = "Please fill in email or phone";
    }
    else if(!empty($_POST['email']))
    {
        $email = $_POST['email'];
        $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
        if(!preg_match($email_exp,$email)) {
          $errors['msg'] = CONTACT_EMAIL_INVALID;
        }
    
    }
    
    if (!empty($errors)) {
		//If errors in validation
    	$form_data['success'] = false;
    	$form_data['errors']  = $errors;
    } else { 
		//If not, process the form, and return true on success
      
      try {
        if($contact->AddContact()) 
        {
    	    $form_data['success'] = true;
        }
        else
        {
    	    $form_data['success'] = false;
          $errors['msg'] = Error;
    	    $form_data['errors']  = $errors;
          $form_data['error_logs'] = $contact->GetErrorMessage();
        }
      }
      catch(Exception $e) {
    	  $form_data['success'] = false;
    	  $form_data['errors'] = Error;
        $form_data['error_logs'] = $e->getMessage();
      }
    }
    
      
    //Return the data back to form.php
    echo json_encode($form_data);

?>