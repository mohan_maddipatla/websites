<?php

/**
 * Class to handle SMS operations
 *
 * @author Satish
 * @link URL Tutorial link
 */
class SMS {

      /**
     * Send SMS
     * @param String $phome
     * @param String @message
     */
    public function SendSMS($phone, $message) {
        include_once dirname(__FILE__) . '/Config.php';
        $response = array();
        try {
          //generate the API call
          //$callURL = SMS_BasePath . SMS_APIPath . '?User=' . SMS_USERNAME . '&passwd=' . SMS_PASSWORD . '&mobilenumber=' . $phone . '&message=' . $message . '&sid=' . SMS_SID . SMS_AdditionalSettings;
          
          //Call api
          // create a new cURL resource
          //$ch = curl_init();
          // set URL and other appropriate options
          //curl_setopt($ch, CURLOPT_URL, $callURL);
          //curl_setopt($ch, CURLOPT_HEADER, 0);
          
          // grab URL and pass it to the browser
          //curl_exec($ch);
          
          // close cURL resource, and free up system resources
          //curl_close($ch);
          
          $getdata = http_build_query(array(
              'User' => SMS_USERNAME,
              'passwd' => SMS_PASSWORD,
              'mobilenumber'=>$phone,
               'message'=>$message,
               'sid'=>SMS_SID,
               'mtype'=>SMS_MType,
               'DR'=>SMS_DR,
           )
          );
          
          file_get_contents(SMS_BasePath . SMS_APIPath . '?' . $getdata);
          
          return 'SMS_SUCCESS';
        }
        catch(Exception $e) {
          return $e->getMessage();
        }
    }
}

?>
