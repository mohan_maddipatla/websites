<?PHP
require_once("surveycontact.php");
require_once(__DIR__."/../Config.php");

$contact = new surveycontact();

//Provide your site name here

//Provide your database login details here:
//hostname, user name, password, database name and table name
//note that the script will create the table (for example, fgusers in this case)
//by itself on submitting register.php for the first time
$contact->InitDB(
        /*hostname*/ DB_HOST, 
        /*username*/ DB_USERNAME, 
        /*password*/ DB_PASSWORD, 
        /*database name*/ DB_NAME, 
        /*table name*/'contacts');

//For better security. Get a random string from this link: http://tinyurl.com/randstr
// and put it here
$contact->SetRandomKey('K50D9unQDdXaPVh');

?>