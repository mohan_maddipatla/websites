<?PHP
require_once("member.php");
require_once(__DIR__."/../Config.php");

$member = new Member();

//Provide your site name here
$member->SetWebsiteName('esahai.in');
$member->SetAppName('eSahai');

//Provide the email address where you want to get notifications
//$member->SetAdminEmail('info@eSahai.in');

//Provide your database login details here:
//hostname, user name, password, database name and table name
//note that the script will create the table (for example, fgusers in this case)
//by itself on submitting register.php for the first time
$member->InitDB(
        /*hostname*/ DB_HOST, 
        /*username*/ DB_USERNAME, 
        /*password*/ DB_PASSWORD, 
        /*database name*/ DB_NAME, 
        /*table name*/'users');

//For better security. Get a random string from this link: http://tinyurl.com/randstr
// and put it here
$member->SetRandomKey('K50D9unQDdXaPVh');

?>