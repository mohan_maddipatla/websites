<?PHP
require_once("formvalidator.php");
require_once(__DIR__."/../../Constants.php");

class surveycontact
{
    var $database;
    var $tablename;
    var $connection;
    var $rand_key;
    var $sitename;
    var $appname;
    var $error_message;
    
    var $allContacts = array();
    var $selectedContact = array();
    
    //-----Initialization -------
    function surveycontact()
    {
        $this->sitename = 'eSahai.in';
        $this->rand_key = 'K50D9unQDdXaPVh';
		    $this->appname = 'eSahai';
    }
    
    function InitDB($host,$uname,$pwd,$database,$tablename)
    {
        $this->db_host  = $host;
        $this->username = $uname;
        $this->pwd  = $pwd;
        $this->database  = $database;
        $this->tablename = $tablename;
        
    }
    
    function SetRandomKey($key)
    {
        $this->rand_key = $key;
    }
    
    //-------Main Operations ----------------------
    function AddContact()
    {
        $formvars = array();
        /*
        if(!$this->ValidateContactSubmission())
        {
            return false;
        }
        */
        $this->CollectContactSubmission($formvars);
        
        if(!$this->SaveContactToDatabase($formvars))
        {
            return false;
        }
        
        return true;
    }

    
    function getAllContacts()
    {
      $this->GetContactsFromDatabase();
    }
    
    //-------Public Helper functions -------------
    function GetSelfScript()
    {
        return htmlentities($_SERVER['PHP_SELF']);
    }
    
    function GetSpamTrapInputName()
    {
        return 'sp'.md5('KHGdnbvsgst'.$this->rand_key);
    }
    
    function GetErrorMessage()
    {
        if(empty($this->error_message))
        {
            return '';
        }
        $errormsg = nl2br(htmlentities($this->error_message));
        return $errormsg;
    }    
    //-------Private Helper functions-----------
    
    function HandleError($err)
    {
        $this->error_message .= $err."\r\n";
    }
    
    function HandleDBError($err)
    {
        $this->HandleError($err."\r\n mysqlerror:".mysql_error());
    }
    
    
    function ValidateContactSubmission()
    {
        //This is a hidden input field. Humans won't fill this field.
        if(!empty($_POST[$this->GetSpamTrapInputName()]) )
        {
            //The proper error is not given intentionally
            $this->HandleError("Automated submission prevention: case 2 failed");
            return false;
        }
        
        $validator = new FormValidator();
        $validator->addValidation("name","req","Please fill in Name");
        $validator->addValidation("email","req","Please fill in Email");
        $validator->addValidation("phone","req","Please fill in Phone");
        
        if(!$validator->ValidateForm())
        {
            $error='';
            $error_hash = $validator->GetErrors();
            foreach($error_hash as $inpname => $inp_err)
            {
                $error .= $inpname.':'.$inp_err."\n";
            }
            $this->HandleError($error);
            return false;
        }        
        return true;
    }
    
    function CollectContactSubmission(&$formvars)
    {
        $formvars['name'] = $this->Sanitize($_POST['name']);
        $formvars['age'] = $this->Sanitize($_POST['age']);
        $formvars['gender'] = $this->Sanitize($_POST['gender']);
        $formvars['email'] = $this->Sanitize($_POST['email']);
        $formvars['phone'] = $this->Sanitize($_POST['phone']);
        $formvars['locality'] = $this->Sanitize($_POST['locality']);
    }
    
    function GetAbsoluteURLFolder()
    {
        $scriptFolder = (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on')) ? 'https://' : 'http://';
        $scriptFolder .= $_SERVER['HTTP_HOST'] . dirname($_SERVER['REQUEST_URI']);
        return $scriptFolder;
    }
    
    function SaveContactToDatabase(&$formvars)
    {
        if(!$this->DBLogin())
        {
            $this->HandleError("Database login failed!");
            return false;
        }
        
        if(isset($formvars['contactid']) && !empty($formvars['contactid']))
        {
          if(!$this->UpdateContactToDB($formvars))
          {
              $this->HandleError("Updating to Database failed!");
              return false;
          }
        }
        else
        {
          if(!$this->InsertContactIntoDB($formvars))
          {
              $this->HandleError("Inserting to Database failed!");
              return false;
          }
        }
        return true;
    }
    
    function DBLogin()
    {

        //$this->connection = mysql_connect($this->db_host,$this->username,$this->pwd);
        $this->connection = new mysqli($this->db_host, $this->username, $this->pwd, $this->database);

        if(!$this->connection)
        {   
            $this->HandleDBError("Database Login failed! Please make sure that the DB login credentials provided are correct");
            return false;
        }
        /*
        if(!mysql_select_db($this->database, $this->connection))
        {
            $this->HandleDBError('Failed to select database: '.$this->database.' Please make sure that the database name provided is correct');
            return false;
        }
        if(!mysql_query("SET NAMES 'UTF8'",$this->connection))
        {
            $this->HandleDBError('Error setting utf8 encoding');
            return false;
        }
        */
        return true;
    }    
    
    function UpdateContactToDB(&$formvars)
    {
        $qry = "update $this->tablename set name='" . $formvars['name'] . 
                "' , age = '"  . $formvars['age'] . 
                "' , gender = '"  . $formvars['gender'] . 
                "' , email = '"  . $formvars['email'] .
                "' , phone = '"  . $formvars['phone'] . 
                "' , locality = '"  . $formvars['locality'] .
                "' where  id='" . $formvars['contactid'] . "'";
        
        if(!mysqli_query( $this->connection, $qry))
        {
            $this->HandleDBError("Error updating the Contact \nquery:$qry");
            return false;
        }     
        return true;
    }

    function InsertContactIntoDB(&$formvars)
    {
        $insert_query = 'insert into ' . $this->tablename . '(name,age,gender,email,phone,locality) values ("' 
                        . $this->SanitizeForSQL($formvars['name']) . '", "' 
                        . $this->SanitizeForSQL($formvars['age']) . '", "' 
                        . $this->SanitizeForSQL($formvars['gender']) . '", "' 
                        . $this->SanitizeForSQL($formvars['email']) . '", "' 
                        . $this->SanitizeForSQL($formvars['phone']) . '", "' 
                        . $this->SanitizeForSQL($formvars['locality']) . '" )';      
        
        if(!mysqli_query( $this->connection, $insert_query))
        {
            $this->HandleDBError("Error inserting data to the table\nquery:$insert_query");
            return false;
        }
        return true;
    }
    
    function GetContactsFromDatabase()
    {
        $query = "select * from Contacts order by id";
        
        if(!$this->DBLogin())
        {
            $this->HandleError("Database login failed!");
            return false;
        }
        
        $result = mysqli_query($this->connection, $query);
        if(!$result)
        {
          return false;
        }
        if ($result->num_rows > 0) {
          while($row = $result->fetch_assoc()) {
            $this->allContacts[] = array($row["id"], $row["name"],$row["age"],$row["gender"],$row["email"], $row["phone"], $row["locality"], $row["crtd_on"]);
          }
        }
        return true;
    }
    
    function GetContactDetailsFromDatabase($contactId)
    {
        $query = "select * from Contacts where id = " . $contactId;
        
        if(!$this->DBLogin())
        {
            $this->HandleError("Database login failed!");
            return false;
        }
        
        $result = mysqli_query($this->connection, $query);
        if(!$result)
        {
          return false;
        }
        if ($result->num_rows > 0) {
          while($row = $result->fetch_assoc()) {
            $this->selectedContact["id"] = $row["id"];
            $this->selectedContact["name"] = $row["name"];
            $this->selectedContact["age"] = $row["age"];
            $this->selectedContact["gender"] = $row["gender"];
            $this->selectedContact["email"] = $row["email"];
            $this->selectedContact["phone"] = $row["phone"];
            $this->selectedContact["locality"] = $row["locality"];
            $this->selectedContact["crtd_on"] = $row["crtd_on"];
          }
        }
        return true;
    }


    function SanitizeForSQL($str)
    {
        if( function_exists( "mysqli_real_escape_string" ) )
        {
              $ret_str = mysqli_real_escape_string( $this->connection, $str );
        }
        else
        {
              $ret_str = addslashes( $str );
        }
        return $ret_str;
    }
    
 /*
    Sanitize() function removes any potential threat from the
    data submitted. Prevents email injections or any other hacker attempts.
    if $remove_nl is true, newline chracters are removed from the input.
    */
    function Sanitize($str,$remove_nl=true)
    {
        $str = $this->StripSlashes($str);

        if($remove_nl)
        {
            $injections = array('/(\n+)/i',
                '/(\r+)/i',
                '/(\t+)/i',
                '/(%0A+)/i',
                '/(%0D+)/i',
                '/(%08+)/i',
                '/(%09+)/i'
                );
            $str = preg_replace($injections,'',$str);
        }

        return $str;
    }    
    function StripSlashes($str)
    {
        if(get_magic_quotes_gpc())
        {
            $str = stripslashes($str);
        }
        return $str;
    }    
}
?>