<?php
/**
 * App Constants
 */
define('Error', 'There is a problem in processing your request. Please try again later.');


 /* Get Link */
define('GET_LINK_REQUIRED', 'Please enter the phone number.');
define('GET_LINK_INVALID', 'Please enter a valid phone number.');
define("GET_LINK_SUCCESS", "Thank you for your interest in eSahai, India's first medical transportation app.");
//define('GET_LINK_SMS', "Thank you for your interest in eSahai, India's first medical transportation app. We are there by your side for your life's crucial most moments. Look out for the app on Playstore with the link... http://market.android.com/details?id=com.esahai&hl=en");
define('GET_LINK_SMS', "Thank you for your interest in eSahai, India's first medical transportation app. Look out for the app on Google Play at bit.ly/2izuR7v");
define('SMS_FAILED', 'Error sending SMS');

/* Subscribe */
define('SUBSCRIBE_REQUIRED', 'Please enter the email address.');
define('SUBSCRIBE_INVALID', 'Please enter a valid email.');
define('SUBSCRIBE_SUCCESS', 'Thank you for showing interest.');
define('SUBSCRIBE_SEND_MAIL_ID', 'eSahai <info@esahai.in>');
define('SUBSCRIBE_EMAIL_SUBJECT', 'Thank you for showing interest in eSahai');
define('SUBSCRIBE_EMAIL_MESSAGE', 'Thank you for showing interest in eSahai');

/* Contact US */
define('CONTACT_MAIL_FROM', 'eSahai <info@esahai.in>');
define('CONTACT_MAIL_TO', 'info@esahai.in');
define('CONTACT_MAIL_SUBJECT', 'Contact mail from esahai');
define('CONTACT_REQUIRED', 'Please enter all the fields.');
define('CONTACT_EMAIL_INVALID', 'Please enter a valid email.');
define('CONTACT_PHONE_INVALID', 'Please enter a valid phone number.');
define('CONTACT_SUCCESS', 'Thank you for contacting us. We will be in touch with you very soon.');
define('SUBSCRIBE_USER_EMAIL_SUBJECT', 'Thank you for contactint us.');

/*admin */
define('ADMIN_EMAIL', 'eSahai <info@esahai.in>');

?>
