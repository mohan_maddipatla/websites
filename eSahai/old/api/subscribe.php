<?php
  require_once 'include/DbHandler.php';
  require_once 'include/Exceptions.php';
  require_once 'Constants.php';
  
	$errors = array(); //To store errors
  $form_data = array(); //Pass back the data to
    
    
    /* Validate the form on server side */
    if (empty($_POST['email'])) { 
		//Email cannot be empty
        $errors['email'] = SUBSCRIBE_REQUIRED;
    }
    
    if (!empty($errors)) {
		//If errors in validation
    	$form_data['success'] = false;
    	$form_data['errors']  = $errors;
    } else { 
		//If not, process the form, and return true on success
      
      $email = $_POST['email'];
      
      try {
        //Save to DB
        $db = new DbHandler();
        $res = $db->Subscribe($email);
        
        if($res == "CREATED_SUCCESSFULLY")
        {
          //Send thankyou mail
          $email_status = False;
        
          $email_to = $email;
          $email_subject = SUBSCRIBE_EMAIL_SUBJECT;
    
          $message = file_get_contents("subscribe_email_template.html");

          // Set content-type header for sending HTML email
          $headers[] = 'MIME-Version: 1.0';
          $headers[] = 'Content-type: text/html; charset=iso-8859-1';

          // Additional headers
          $headers[] = 'From: '. SUBSCRIBE_SEND_MAIL_ID;
        
          // Send email
          if(mail($email_to, $email_subject, $message, implode("\r\n", $headers)))
          {
    	      $form_data['success'] = true;
    	      $form_data['posted']  = SUBSCRIBE_SUCCESS;
            $email_status = True;
          }
          else
          {
            $mail_error = 'unable to send mail'; //error_get_last();
    	      $form_data['success'] = false;
            $errors['error'] = Error;
            $form_data['errors']  = $errors;
            $form_data['error_logs'] = $mail_error;
          }
        }
        else {
            $mail_error = 'unable to save to database'; //error_get_last();
    	      $form_data['success'] = false;
            $errors['error'] = Error;
            $form_data['errors']  = $errors;
            $form_data['error_logs'] = $mail_error;
        }
      }
      catch(Exception $e) {
    	  $form_data['success'] = false;
    	  $form_data['errors'] = Error;
        $form_data['error_logs'] = $e->getMessage();
      }
    }
    
      
    //Return the data back to form.php
    echo json_encode($form_data);

?>