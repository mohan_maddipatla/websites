<?php
  require_once 'include/DbHandler.php';
  require_once 'include/Exceptions.php';
  require_once 'include/SMS.php';
  require_once 'Constants.php';
  
	$errors = array(); //To store errors
    $form_data = array(); //Pass back the data to
    
    
    /* Validate the form on server side */
    if (empty($_POST['phone'])) { 
		//Email cannot be empty
        $errors['phone'] = GET_LINK_REQUIRED;
    }
    else if(!preg_match('/^[0-9]{10}$/', $_POST['phone'])) {
        $errors['phone'] = GET_LINK_INVALID;
    }
    
    if (!empty($errors)) {
		//If errors in validation
    	$form_data['success'] = false;
    	$form_data['errors']  = $errors;
    } else { 
		//If not, process the form, and return true on success
      
      $phone = $_POST['phone'];
      $message = GET_LINK_SMS;
      try
      {
        //Save to DB
        $db = new DbHandler();
        $res = $db->GetLink($phone);
      
        if($res == "CREATED_SUCCESSFULLY") {
        
          //Send SMS
          $smsObj = new SMS();
          $sms_res = $smsObj->SendSMS($phone, $message);
          if($sms_res == 'SMS_SUCCESS') {
    	      $form_data['success'] = true;
    	      $form_data['posted'] = GET_LINK_SUCCESS;
            //$form_data['error_logs'] = $sms_res;
          }
          else {
    	      $form_data['success'] = false;
    	      $form_data['posted'] = Error;
            $form_data['error_logs'] = $sms_res;
          }
        }
        else {
          $mail_error = 'unable to save to database'; //error_get_last();
    	    $form_data['success'] = false;
          $errors['error'] = Error;
          $form_data['errors']  = $errors;
          $form_data['error_logs'] = $mail_error;
        }
      }
      catch(Exception $e) {
    	  $form_data['success'] = false;
    	  $form_data['errors'] = Error;
        $form_data['error_logs'] = $e->getMessage();
      }
    }

    //Return the data back to form.php
    echo json_encode($form_data);

?>