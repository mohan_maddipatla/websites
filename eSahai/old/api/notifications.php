<?php
  require_once 'include/DbHandler.php';
  require_once 'include/Exceptions.php';
  require_once 'Constants.php';
  
	$errors = array(); //To store errors
    $form_data = array(); //Pass back the data to
    
    
    /* Validate the form on server side */
    
    if (!empty($errors)) {
		//If errors in validation
    	$form_data['success'] = false;
    	$form_data['errors']  = $errors;
    } else { 
		//If not, process the form, and return true on success
      
      try
      {
        //Get active notification
        $db = new DbHandler();
        $res = $db->GetNotification();
        
        if($res['rows'] > 0)
        {
          $stDate = trim($res['start_date']);
          $enDate = trim($res['end_date']);
        
          $res['showAdd'] = false;
        
          if(empty($stDate) && empty($enDate)) {
            $res['showAdd'] = true;
          }
          else {
            $format = 'd/m/Y';
            $today = new DateTime('now');
            if(empty($stDate) && !empty($enDate)) {
              $date1 = DateTime::createFromFormat($format, $enDate);
              if($today <= $date1)
                $res['showAdd'] = true;
            }
            else if (!empty($stDate) && empty($enDate)) {
              $date1 = DateTime::createFromFormat($format, $stDate);
              if($date1 <= $today)
                $res['showAdd'] = true;
            }
            else {
              $date1 = DateTime::createFromFormat($format, $stDate);
              $date2 = DateTime::createFromFormat($format, $enDate);
              if($date1 <= $today && $today <= $date2)
                $res['showAdd'] = true;
            }
          }
        }
    	  $form_data['success'] = true;
    	  $form_data['posted'] = $res;
      }
      catch(Exception $e) {
    	  $form_data['success'] = false;
    	  $form_data['errors'] = Error;
        $form_data['error_logs'] = $e->getMessage();
      }
    }

    //Return the data back to form.php
    echo json_encode($form_data);

?>