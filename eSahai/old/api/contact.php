<?php
    require_once 'include/Exceptions.php';
    require_once 'Constants.php';
    
	  $errors = array(); //To store errors
    $form_data = array(); //Pass back the data to
    
    $email_to = CONTACT_MAIL_TO;
    $email_subject = CONTACT_MAIL_SUBJECT;
    
    if( !(isset($_POST['name']) && isset($_POST['message'])))
    {
      $errors['error'] = CONTACT_REQUIRED;
    }
    else
    {
      $name_form = $_POST['name'];    // required
      $email_from = $_POST['email'];  // required
      $phone = $_POST['phone'];       // not required
      $message = $_POST['message'];   // required

      /* Validate the form on server side */
      if (empty($_POST['name']) || 
          empty($_POST['email']) ||
          empty($_POST['message'])) {
          $errors['error'] = CONTACT_REQUIRED;
      }
      else
      {
        if(!empty($_POST['email']))
        {
          $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
          if(!preg_match($email_exp,$email_from)) {
            $errors['email'] = CONTACT_EMAIL_INVALID;
          }
        }

        if(!empty($_POST['phone']))
        {
          if(!preg_match('/^[0-9]{10}$/', $phone)) {
              $errors['phone'] = CONTACT_PHONE_INVALID;
          }
        }
      }
    }
    function clean_string($string) {
      $bad = array("content-type","bcc:","to:","cc:","href");
      return str_replace($bad,"",$string);
    }
    
    if (!empty($errors)) {
		  //If errors in validation
    	$form_data['success'] = false;
    	$form_data['errors']  = $errors;
    } else { 
		  //If not, process the form, and return true on success
      
      //Send the same mail to the user
      $email_to .= ','. $email_from;
      
      try {
        $email_message = '<html><head></head><body><table><tr><td><b>Name :</b></td><td>' .clean_string($name_form). '</td></tr>';
        $email_message .= '<tr><td><b>Email :</b></td><td>'.clean_string($email_from).'</td></tr>';
        $email_message .= '<tr><td><b>Phone :</b></td><td>'.clean_string($phone).'</td></tr>';
        $email_message .= '<tr><td><b>Message :</b></td><td>'.clean_string($message).'</td></tr>';
        $email_message .= '</table></body></html>';
        
        // create email headers
        $headers[] = 'MIME-Version: 1.0';
        $headers[] = 'Content-type: text/html; charset=iso-8859-1';

        // Additional headers
        $headers[] = 'From: '. CONTACT_MAIL_FROM;
        
        //Send mail to the host.
        if(mail($email_to, $email_subject, $email_message, implode("\r\n", $headers)))
        {
    	    $form_data['success'] = true;
    	    $form_data['posted']  = CONTACT_SUCCESS;
        }
        else
        {
          $mail_error = 'unable to send mail'; //error_get_last();
    	    $form_data['success'] = false;
          $errors['error'] = Error;
          $form_data['errors']  = $errors;
          $form_data['error_logs'] = $mail_error;
        }
       }
       catch(Exception $e) {
    	  $form_data['success'] = false;
        $errors['error'] = Error;
    	  $form_data['errors']  = $errors;
        $form_data['error_logs'] = $e->getMessage();
       }
    }

    //Return the data back to form.php
    echo json_encode($form_data);

?>